use glib::prelude::*;
use glib::translate::*;

use crate::VulkanDevice;

use ffi::GstVulkanBufferMemory;
use gst::{Memory, MemoryRef};

use std::fmt;

// gst::mini_object_wrapper!(
//     VulkanBufferMemory,
//     VulkanBufferMemoryRef,
//     GstVulkanBufferMemory
// );

gst::memory_object_wrapper!(
    VulkanBufferMemory,
    VulkanBufferMemoryRef,
    GstVulkanBufferMemory,
    |mem: &MemoryRef| { unsafe { from_glib(ffi::gst_is_vulkan_buffer_memory(mem.as_mut_ptr())) } },
    // VulkanMemory,
    // VulkanMemoryRef,
    Memory,
    MemoryRef
);

impl fmt::Debug for VulkanBufferMemory {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        VulkanBufferMemoryRef::fmt(self, f)
    }
}

impl fmt::Debug for VulkanBufferMemoryRef {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        MemoryRef::fmt(self, f)
    }
}

// impl std::ops::Deref for VulkanBufferMemoryRef {
//     type Target = MemoryRef;

//     fn deref(&self) -> &Self::Target {
//         unsafe { &*(&self.0.parent as *const GstMemory).cast::<Self::Target>() }
//     }
// }

// impl std::ops::DerefMut for VulkanBufferMemoryRef {
//     fn deref_mut(&mut self) -> &mut Self::Target {
//         unsafe { &mut *(&mut self.0.parent as *mut GstMemory).cast::<Self::Target>() }
//     }
// }

impl VulkanBufferMemory {
    #[doc(alias = "gst_vulkan_buffer_memory_alloc")]
    pub fn alloc(
        device: &impl IsA<VulkanDevice>,
        size: usize,
        usage: vulkan::BufferUsageFlags,
        mem_prop_flags: vulkan::MemoryPropertyFlags,
    ) -> gst::Memory {
        skip_assert_initialized!();
        Self::init_once();
        unsafe {
            from_glib_full(ffi::gst_vulkan_buffer_memory_alloc(
                device.as_ref().to_glib_none().0,
                size,
                usage,
                mem_prop_flags,
            ))
        }
    }

    #[cfg(feature = "v1_24")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
    #[doc(alias = "gst_vulkan_buffer_memory_alloc_with_buffer_info")]
    pub fn alloc_with_buffer_info(
        device: &impl IsA<VulkanDevice>,
        buffer_info: &vulkan::BufferCreateInfo,
        mem_prop_flags: vulkan::MemoryPropertyFlags,
    ) -> gst::Memory {
        skip_assert_initialized!();
        Self::init_once();
        unsafe {
            from_glib_full(ffi::gst_vulkan_buffer_memory_alloc_with_buffer_info(
                device.as_ref().to_glib_none().0,
                buffer_info,
                mem_prop_flags,
            ))
        }
    }

    #[doc(alias = "gst_vulkan_buffer_memory_init_once")]
    fn init_once() {
        assert_initialized_main_thread!();
        unsafe {
            ffi::gst_vulkan_buffer_memory_init_once();
        }
    }

    //#[doc(alias = "gst_vulkan_buffer_memory_wrapped")]
    //pub fn wrapped(device: &impl IsA<VulkanDevice>, buffer: vulkan::Buffer, usage: vulkan::BufferUsageFlags, user_data: /*Unimplemented*/Option<Basic: Pointer>) -> gst::Memory {
    //    unsafe { TODO: call ffi:gst_vulkan_buffer_memory_wrapped() }
    //}
}
