// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// from gst-gir-files (https://gitlab.freedesktop.org/gstreamer/gir-files-rs.git)
// DO NOT EDIT

use crate::{VulkanDevice, VulkanQueue, VulkanWindow};
use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::boxed::Box as Box_;

glib::wrapper! {
    #[doc(alias = "GstVulkanSwapper")]
    pub struct VulkanSwapper(Object<ffi::GstVulkanSwapper, ffi::GstVulkanSwapperClass>) @extends gst::Object;

    match fn {
        type_ => || ffi::gst_vulkan_swapper_get_type(),
    }
}

impl VulkanSwapper {
    pub const NONE: Option<&'static VulkanSwapper> = None;

    #[doc(alias = "gst_vulkan_swapper_new")]
    pub fn new(device: &impl IsA<VulkanDevice>, window: &impl IsA<VulkanWindow>) -> VulkanSwapper {
        skip_assert_initialized!();
        unsafe {
            from_glib_none(ffi::gst_vulkan_swapper_new(
                device.as_ref().to_glib_none().0,
                window.as_ref().to_glib_none().0,
            ))
        }
    }
}

unsafe impl Send for VulkanSwapper {}
unsafe impl Sync for VulkanSwapper {}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::VulkanSwapper>> Sealed for T {}
}

pub trait VulkanSwapperExt: IsA<VulkanSwapper> + sealed::Sealed + 'static {
    #[doc(alias = "gst_vulkan_swapper_choose_queue")]
    fn choose_queue(&self, available_queue: &impl IsA<VulkanQueue>) -> Result<(), glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let is_ok = ffi::gst_vulkan_swapper_choose_queue(
                self.as_ref().to_glib_none().0,
                available_queue.as_ref().to_glib_none().0,
                &mut error,
            );
            debug_assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[doc(alias = "gst_vulkan_swapper_get_supported_caps")]
    #[doc(alias = "get_supported_caps")]
    fn supported_caps(&self) -> Result<gst::Caps, glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let ret = ffi::gst_vulkan_swapper_get_supported_caps(
                self.as_ref().to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[doc(alias = "gst_vulkan_swapper_render_buffer")]
    fn render_buffer(&self, buffer: &gst::Buffer) -> Result<(), glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let is_ok = ffi::gst_vulkan_swapper_render_buffer(
                self.as_ref().to_glib_none().0,
                buffer.to_glib_none().0,
                &mut error,
            );
            debug_assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[doc(alias = "gst_vulkan_swapper_set_caps")]
    fn set_caps(&self, caps: &gst::Caps) -> Result<(), glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let is_ok = ffi::gst_vulkan_swapper_set_caps(
                self.as_ref().to_glib_none().0,
                caps.to_glib_none().0,
                &mut error,
            );
            debug_assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[doc(alias = "force-aspect-ratio")]
    fn is_force_aspect_ratio(&self) -> bool {
        ObjectExt::property(self.as_ref(), "force-aspect-ratio")
    }

    #[doc(alias = "force-aspect-ratio")]
    fn set_force_aspect_ratio(&self, force_aspect_ratio: bool) {
        ObjectExt::set_property(self.as_ref(), "force-aspect-ratio", force_aspect_ratio)
    }

    #[doc(alias = "pixel-aspect-ratio")]
    fn pixel_aspect_ratio(&self) -> gst::Fraction {
        ObjectExt::property(self.as_ref(), "pixel-aspect-ratio")
    }

    #[doc(alias = "pixel-aspect-ratio")]
    fn set_pixel_aspect_ratio(&self, pixel_aspect_ratio: gst::Fraction) {
        ObjectExt::set_property(self.as_ref(), "pixel-aspect-ratio", pixel_aspect_ratio)
    }

    #[doc(alias = "force-aspect-ratio")]
    fn connect_force_aspect_ratio_notify<F: Fn(&Self) + Send + Sync + 'static>(
        &self,
        f: F,
    ) -> SignalHandlerId {
        unsafe extern "C" fn notify_force_aspect_ratio_trampoline<
            P: IsA<VulkanSwapper>,
            F: Fn(&P) + Send + Sync + 'static,
        >(
            this: *mut ffi::GstVulkanSwapper,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(VulkanSwapper::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::force-aspect-ratio\0".as_ptr() as *const _,
                Some(std::mem::transmute::<_, unsafe extern "C" fn()>(
                    notify_force_aspect_ratio_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "pixel-aspect-ratio")]
    fn connect_pixel_aspect_ratio_notify<F: Fn(&Self) + Send + Sync + 'static>(
        &self,
        f: F,
    ) -> SignalHandlerId {
        unsafe extern "C" fn notify_pixel_aspect_ratio_trampoline<
            P: IsA<VulkanSwapper>,
            F: Fn(&P) + Send + Sync + 'static,
        >(
            this: *mut ffi::GstVulkanSwapper,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(VulkanSwapper::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::pixel-aspect-ratio\0".as_ptr() as *const _,
                Some(std::mem::transmute::<_, unsafe extern "C" fn()>(
                    notify_pixel_aspect_ratio_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl<O: IsA<VulkanSwapper>> VulkanSwapperExt for O {}
