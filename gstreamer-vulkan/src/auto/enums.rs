// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// from gst-gir-files (https://gitlab.freedesktop.org/gstreamer/gir-files-rs.git)
// DO NOT EDIT

use glib::{prelude::*, translate::*};

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Clone, Copy)]
#[non_exhaustive]
#[doc(alias = "GstVulkanBarrierFlags")]
pub enum VulkanBarrierFlags {
    #[doc(alias = "GST_VULKAN_BARRIER_FLAG_NONE")]
    None,
    #[doc(hidden)]
    __Unknown(i32),
}

#[doc(hidden)]
impl IntoGlib for VulkanBarrierFlags {
    type GlibType = ffi::GstVulkanBarrierFlags;

    #[inline]
    fn into_glib(self) -> ffi::GstVulkanBarrierFlags {
        match self {
            Self::None => ffi::GST_VULKAN_BARRIER_FLAG_NONE,
            Self::__Unknown(value) => value,
        }
    }
}

#[doc(hidden)]
impl FromGlib<ffi::GstVulkanBarrierFlags> for VulkanBarrierFlags {
    #[inline]
    unsafe fn from_glib(value: ffi::GstVulkanBarrierFlags) -> Self {
        skip_assert_initialized!();

        match value {
            ffi::GST_VULKAN_BARRIER_FLAG_NONE => Self::None,
            value => Self::__Unknown(value),
        }
    }
}

impl StaticType for VulkanBarrierFlags {
    #[inline]
    #[doc(alias = "gst_vulkan_barrier_flags_get_type")]
    fn static_type() -> glib::Type {
        unsafe { from_glib(ffi::gst_vulkan_barrier_flags_get_type()) }
    }
}

impl glib::HasParamSpec for VulkanBarrierFlags {
    type ParamSpec = glib::ParamSpecEnum;
    type SetValue = Self;
    type BuilderFn = fn(&str, Self) -> glib::ParamSpecEnumBuilder<Self>;

    fn param_spec_builder() -> Self::BuilderFn {
        Self::ParamSpec::builder_with_default
    }
}

impl glib::value::ValueType for VulkanBarrierFlags {
    type Type = Self;
}

unsafe impl<'a> glib::value::FromValue<'a> for VulkanBarrierFlags {
    type Checker = glib::value::GenericValueTypeChecker<Self>;

    #[inline]
    unsafe fn from_value(value: &'a glib::Value) -> Self {
        skip_assert_initialized!();
        from_glib(glib::gobject_ffi::g_value_get_enum(value.to_glib_none().0))
    }
}

impl ToValue for VulkanBarrierFlags {
    #[inline]
    fn to_value(&self) -> glib::Value {
        let mut value = glib::Value::for_value_type::<Self>();
        unsafe {
            glib::gobject_ffi::g_value_set_enum(value.to_glib_none_mut().0, self.into_glib());
        }
        value
    }

    #[inline]
    fn value_type(&self) -> glib::Type {
        Self::static_type()
    }
}

impl From<VulkanBarrierFlags> for glib::Value {
    #[inline]
    fn from(v: VulkanBarrierFlags) -> Self {
        skip_assert_initialized!();
        ToValue::to_value(&v)
    }
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Clone, Copy)]
#[non_exhaustive]
#[doc(alias = "GstVulkanBarrierType")]
pub enum VulkanBarrierType {
    #[doc(alias = "GST_VULKAN_BARRIER_NONE")]
    None,
    #[doc(alias = "GST_VULKAN_BARRIER_TYPE_MEMORY")]
    TypeMemory,
    #[doc(alias = "GST_VULKAN_BARRIER_TYPE_BUFFER")]
    TypeBuffer,
    #[doc(alias = "GST_VULKAN_BARRIER_TYPE_IMAGE")]
    TypeImage,
    #[doc(hidden)]
    __Unknown(i32),
}

#[doc(hidden)]
impl IntoGlib for VulkanBarrierType {
    type GlibType = ffi::GstVulkanBarrierType;

    #[inline]
    fn into_glib(self) -> ffi::GstVulkanBarrierType {
        match self {
            Self::None => ffi::GST_VULKAN_BARRIER_NONE,
            Self::TypeMemory => ffi::GST_VULKAN_BARRIER_TYPE_MEMORY,
            Self::TypeBuffer => ffi::GST_VULKAN_BARRIER_TYPE_BUFFER,
            Self::TypeImage => ffi::GST_VULKAN_BARRIER_TYPE_IMAGE,
            Self::__Unknown(value) => value,
        }
    }
}

#[doc(hidden)]
impl FromGlib<ffi::GstVulkanBarrierType> for VulkanBarrierType {
    #[inline]
    unsafe fn from_glib(value: ffi::GstVulkanBarrierType) -> Self {
        skip_assert_initialized!();

        match value {
            ffi::GST_VULKAN_BARRIER_NONE => Self::None,
            ffi::GST_VULKAN_BARRIER_TYPE_MEMORY => Self::TypeMemory,
            ffi::GST_VULKAN_BARRIER_TYPE_BUFFER => Self::TypeBuffer,
            ffi::GST_VULKAN_BARRIER_TYPE_IMAGE => Self::TypeImage,
            value => Self::__Unknown(value),
        }
    }
}

impl StaticType for VulkanBarrierType {
    #[inline]
    #[doc(alias = "gst_vulkan_barrier_type_get_type")]
    fn static_type() -> glib::Type {
        unsafe { from_glib(ffi::gst_vulkan_barrier_type_get_type()) }
    }
}

impl glib::HasParamSpec for VulkanBarrierType {
    type ParamSpec = glib::ParamSpecEnum;
    type SetValue = Self;
    type BuilderFn = fn(&str, Self) -> glib::ParamSpecEnumBuilder<Self>;

    fn param_spec_builder() -> Self::BuilderFn {
        Self::ParamSpec::builder_with_default
    }
}

impl glib::value::ValueType for VulkanBarrierType {
    type Type = Self;
}

unsafe impl<'a> glib::value::FromValue<'a> for VulkanBarrierType {
    type Checker = glib::value::GenericValueTypeChecker<Self>;

    #[inline]
    unsafe fn from_value(value: &'a glib::Value) -> Self {
        skip_assert_initialized!();
        from_glib(glib::gobject_ffi::g_value_get_enum(value.to_glib_none().0))
    }
}

impl ToValue for VulkanBarrierType {
    #[inline]
    fn to_value(&self) -> glib::Value {
        let mut value = glib::Value::for_value_type::<Self>();
        unsafe {
            glib::gobject_ffi::g_value_set_enum(value.to_glib_none_mut().0, self.into_glib());
        }
        value
    }

    #[inline]
    fn value_type(&self) -> glib::Type {
        Self::static_type()
    }
}

impl From<VulkanBarrierType> for glib::Value {
    #[inline]
    fn from(v: VulkanBarrierType) -> Self {
        skip_assert_initialized!();
        ToValue::to_value(&v)
    }
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Clone, Copy)]
#[non_exhaustive]
#[doc(alias = "GstVulkanError")]
pub enum VulkanError {
    #[doc(alias = "GST_VULKAN_FAILED")]
    Failed,
    #[doc(hidden)]
    __Unknown(i32),
}

impl VulkanError {
    //#[doc(alias = "gst_vulkan_error_to_g_error")]
    //pub fn to_g_error(result: vulkan::Result, format: &str, : /*Unknown conversion*//*Unimplemented*/Basic: VarArgs) -> (vulkan::Result, glib::Error) {
    //    unsafe { TODO: call ffi:gst_vulkan_error_to_g_error() }
    //}
}

#[doc(hidden)]
impl IntoGlib for VulkanError {
    type GlibType = ffi::GstVulkanError;

    #[inline]
    fn into_glib(self) -> ffi::GstVulkanError {
        match self {
            Self::Failed => ffi::GST_VULKAN_FAILED,
            Self::__Unknown(value) => value,
        }
    }
}

#[doc(hidden)]
impl FromGlib<ffi::GstVulkanError> for VulkanError {
    #[inline]
    unsafe fn from_glib(value: ffi::GstVulkanError) -> Self {
        skip_assert_initialized!();

        match value {
            ffi::GST_VULKAN_FAILED => Self::Failed,
            value => Self::__Unknown(value),
        }
    }
}

impl glib::error::ErrorDomain for VulkanError {
    #[inline]
    fn domain() -> glib::Quark {
        skip_assert_initialized!();

        unsafe { from_glib(ffi::gst_vulkan_error_quark()) }
    }

    #[inline]
    fn code(self) -> i32 {
        self.into_glib()
    }

    #[inline]
    #[allow(clippy::match_single_binding)]
    fn from(code: i32) -> Option<Self> {
        skip_assert_initialized!();
        match unsafe { from_glib(code) } {
            Self::__Unknown(_) => Some(Self::Failed),
            value => Some(value),
        }
    }
}

impl StaticType for VulkanError {
    #[inline]
    #[doc(alias = "gst_vulkan_error_get_type")]
    fn static_type() -> glib::Type {
        unsafe { from_glib(ffi::gst_vulkan_error_get_type()) }
    }
}

impl glib::HasParamSpec for VulkanError {
    type ParamSpec = glib::ParamSpecEnum;
    type SetValue = Self;
    type BuilderFn = fn(&str, Self) -> glib::ParamSpecEnumBuilder<Self>;

    fn param_spec_builder() -> Self::BuilderFn {
        Self::ParamSpec::builder_with_default
    }
}

impl glib::value::ValueType for VulkanError {
    type Type = Self;
}

unsafe impl<'a> glib::value::FromValue<'a> for VulkanError {
    type Checker = glib::value::GenericValueTypeChecker<Self>;

    #[inline]
    unsafe fn from_value(value: &'a glib::Value) -> Self {
        skip_assert_initialized!();
        from_glib(glib::gobject_ffi::g_value_get_enum(value.to_glib_none().0))
    }
}

impl ToValue for VulkanError {
    #[inline]
    fn to_value(&self) -> glib::Value {
        let mut value = glib::Value::for_value_type::<Self>();
        unsafe {
            glib::gobject_ffi::g_value_set_enum(value.to_glib_none_mut().0, self.into_glib());
        }
        value
    }

    #[inline]
    fn value_type(&self) -> glib::Type {
        Self::static_type()
    }
}

impl From<VulkanError> for glib::Value {
    #[inline]
    fn from(v: VulkanError) -> Self {
        skip_assert_initialized!();
        ToValue::to_value(&v)
    }
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Clone, Copy)]
#[non_exhaustive]
#[doc(alias = "GstVulkanFormatScaling")]
pub enum VulkanFormatScaling {
    #[doc(alias = "GST_VULKAN_FORMAT_SCALING_UNORM")]
    Unorm,
    #[doc(alias = "GST_VULKAN_FORMAT_SCALING_SNORM")]
    Snorm,
    #[doc(alias = "GST_VULKAN_FORMAT_SCALING_USCALED")]
    Uscaled,
    #[doc(alias = "GST_VULKAN_FORMAT_SCALING_SSCALED")]
    Sscaled,
    #[doc(alias = "GST_VULKAN_FORMAT_SCALING_UINT")]
    Uint,
    #[doc(alias = "GST_VULKAN_FORMAT_SCALING_SINT")]
    Sint,
    #[doc(alias = "GST_VULKAN_FORMAT_SCALING_SRGB")]
    Srgb,
    #[doc(hidden)]
    __Unknown(i32),
}

#[doc(hidden)]
impl IntoGlib for VulkanFormatScaling {
    type GlibType = ffi::GstVulkanFormatScaling;

    #[inline]
    fn into_glib(self) -> ffi::GstVulkanFormatScaling {
        match self {
            Self::Unorm => ffi::GST_VULKAN_FORMAT_SCALING_UNORM,
            Self::Snorm => ffi::GST_VULKAN_FORMAT_SCALING_SNORM,
            Self::Uscaled => ffi::GST_VULKAN_FORMAT_SCALING_USCALED,
            Self::Sscaled => ffi::GST_VULKAN_FORMAT_SCALING_SSCALED,
            Self::Uint => ffi::GST_VULKAN_FORMAT_SCALING_UINT,
            Self::Sint => ffi::GST_VULKAN_FORMAT_SCALING_SINT,
            Self::Srgb => ffi::GST_VULKAN_FORMAT_SCALING_SRGB,
            Self::__Unknown(value) => value,
        }
    }
}

#[doc(hidden)]
impl FromGlib<ffi::GstVulkanFormatScaling> for VulkanFormatScaling {
    #[inline]
    unsafe fn from_glib(value: ffi::GstVulkanFormatScaling) -> Self {
        skip_assert_initialized!();

        match value {
            ffi::GST_VULKAN_FORMAT_SCALING_UNORM => Self::Unorm,
            ffi::GST_VULKAN_FORMAT_SCALING_SNORM => Self::Snorm,
            ffi::GST_VULKAN_FORMAT_SCALING_USCALED => Self::Uscaled,
            ffi::GST_VULKAN_FORMAT_SCALING_SSCALED => Self::Sscaled,
            ffi::GST_VULKAN_FORMAT_SCALING_UINT => Self::Uint,
            ffi::GST_VULKAN_FORMAT_SCALING_SINT => Self::Sint,
            ffi::GST_VULKAN_FORMAT_SCALING_SRGB => Self::Srgb,
            value => Self::__Unknown(value),
        }
    }
}

impl StaticType for VulkanFormatScaling {
    #[inline]
    #[doc(alias = "gst_vulkan_format_scaling_get_type")]
    fn static_type() -> glib::Type {
        unsafe { from_glib(ffi::gst_vulkan_format_scaling_get_type()) }
    }
}

impl glib::HasParamSpec for VulkanFormatScaling {
    type ParamSpec = glib::ParamSpecEnum;
    type SetValue = Self;
    type BuilderFn = fn(&str, Self) -> glib::ParamSpecEnumBuilder<Self>;

    fn param_spec_builder() -> Self::BuilderFn {
        Self::ParamSpec::builder_with_default
    }
}

impl glib::value::ValueType for VulkanFormatScaling {
    type Type = Self;
}

unsafe impl<'a> glib::value::FromValue<'a> for VulkanFormatScaling {
    type Checker = glib::value::GenericValueTypeChecker<Self>;

    #[inline]
    unsafe fn from_value(value: &'a glib::Value) -> Self {
        skip_assert_initialized!();
        from_glib(glib::gobject_ffi::g_value_get_enum(value.to_glib_none().0))
    }
}

impl ToValue for VulkanFormatScaling {
    #[inline]
    fn to_value(&self) -> glib::Value {
        let mut value = glib::Value::for_value_type::<Self>();
        unsafe {
            glib::gobject_ffi::g_value_set_enum(value.to_glib_none_mut().0, self.into_glib());
        }
        value
    }

    #[inline]
    fn value_type(&self) -> glib::Type {
        Self::static_type()
    }
}

impl From<VulkanFormatScaling> for glib::Value {
    #[inline]
    fn from(v: VulkanFormatScaling) -> Self {
        skip_assert_initialized!();
        ToValue::to_value(&v)
    }
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Clone, Copy)]
#[non_exhaustive]
#[doc(alias = "GstVulkanHandleType")]
pub enum VulkanHandleType {
    #[doc(alias = "GST_VULKAN_HANDLE_TYPE_DESCRIPTOR_SET_LAYOUT")]
    DescriptorSetLayout,
    #[doc(alias = "GST_VULKAN_HANDLE_TYPE_PIPELINE_LAYOUT")]
    PipelineLayout,
    #[doc(alias = "GST_VULKAN_HANDLE_TYPE_PIPELINE")]
    Pipeline,
    #[doc(alias = "GST_VULKAN_HANDLE_TYPE_RENDER_PASS")]
    RenderPass,
    #[doc(alias = "GST_VULKAN_HANDLE_TYPE_SAMPLER")]
    Sampler,
    #[doc(alias = "GST_VULKAN_HANDLE_TYPE_FRAMEBUFFER")]
    Framebuffer,
    #[doc(alias = "GST_VULKAN_HANDLE_TYPE_SHADER")]
    Shader,
    #[cfg(feature = "v1_24")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
    #[doc(alias = "GST_VULKAN_HANDLE_TYPE_VIDEO_SESSION")]
    VideoSession,
    #[cfg(feature = "v1_24")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
    #[doc(alias = "GST_VULKAN_HANDLE_TYPE_VIDEO_SESSION_PARAMETERS")]
    VideoSessionParameters,
    #[cfg(feature = "v1_24")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
    #[doc(alias = "GST_VULKAN_HANDLE_TYPE_SAMPLER_YCBCR_CONVERSION")]
    SamplerYcbcrConversion,
    #[doc(hidden)]
    __Unknown(i32),
}

#[doc(hidden)]
impl IntoGlib for VulkanHandleType {
    type GlibType = ffi::GstVulkanHandleType;

    #[inline]
    fn into_glib(self) -> ffi::GstVulkanHandleType {
        match self {
            Self::DescriptorSetLayout => ffi::GST_VULKAN_HANDLE_TYPE_DESCRIPTOR_SET_LAYOUT,
            Self::PipelineLayout => ffi::GST_VULKAN_HANDLE_TYPE_PIPELINE_LAYOUT,
            Self::Pipeline => ffi::GST_VULKAN_HANDLE_TYPE_PIPELINE,
            Self::RenderPass => ffi::GST_VULKAN_HANDLE_TYPE_RENDER_PASS,
            Self::Sampler => ffi::GST_VULKAN_HANDLE_TYPE_SAMPLER,
            Self::Framebuffer => ffi::GST_VULKAN_HANDLE_TYPE_FRAMEBUFFER,
            Self::Shader => ffi::GST_VULKAN_HANDLE_TYPE_SHADER,
            #[cfg(feature = "v1_24")]
            Self::VideoSession => ffi::GST_VULKAN_HANDLE_TYPE_VIDEO_SESSION,
            #[cfg(feature = "v1_24")]
            Self::VideoSessionParameters => ffi::GST_VULKAN_HANDLE_TYPE_VIDEO_SESSION_PARAMETERS,
            #[cfg(feature = "v1_24")]
            Self::SamplerYcbcrConversion => ffi::GST_VULKAN_HANDLE_TYPE_SAMPLER_YCBCR_CONVERSION,
            Self::__Unknown(value) => value,
        }
    }
}

#[doc(hidden)]
impl FromGlib<ffi::GstVulkanHandleType> for VulkanHandleType {
    #[inline]
    unsafe fn from_glib(value: ffi::GstVulkanHandleType) -> Self {
        skip_assert_initialized!();

        match value {
            ffi::GST_VULKAN_HANDLE_TYPE_DESCRIPTOR_SET_LAYOUT => Self::DescriptorSetLayout,
            ffi::GST_VULKAN_HANDLE_TYPE_PIPELINE_LAYOUT => Self::PipelineLayout,
            ffi::GST_VULKAN_HANDLE_TYPE_PIPELINE => Self::Pipeline,
            ffi::GST_VULKAN_HANDLE_TYPE_RENDER_PASS => Self::RenderPass,
            ffi::GST_VULKAN_HANDLE_TYPE_SAMPLER => Self::Sampler,
            ffi::GST_VULKAN_HANDLE_TYPE_FRAMEBUFFER => Self::Framebuffer,
            ffi::GST_VULKAN_HANDLE_TYPE_SHADER => Self::Shader,
            #[cfg(feature = "v1_24")]
            ffi::GST_VULKAN_HANDLE_TYPE_VIDEO_SESSION => Self::VideoSession,
            #[cfg(feature = "v1_24")]
            ffi::GST_VULKAN_HANDLE_TYPE_VIDEO_SESSION_PARAMETERS => Self::VideoSessionParameters,
            #[cfg(feature = "v1_24")]
            ffi::GST_VULKAN_HANDLE_TYPE_SAMPLER_YCBCR_CONVERSION => Self::SamplerYcbcrConversion,
            value => Self::__Unknown(value),
        }
    }
}

impl StaticType for VulkanHandleType {
    #[inline]
    #[doc(alias = "gst_vulkan_handle_type_get_type")]
    fn static_type() -> glib::Type {
        unsafe { from_glib(ffi::gst_vulkan_handle_type_get_type()) }
    }
}

impl glib::HasParamSpec for VulkanHandleType {
    type ParamSpec = glib::ParamSpecEnum;
    type SetValue = Self;
    type BuilderFn = fn(&str, Self) -> glib::ParamSpecEnumBuilder<Self>;

    fn param_spec_builder() -> Self::BuilderFn {
        Self::ParamSpec::builder_with_default
    }
}

impl glib::value::ValueType for VulkanHandleType {
    type Type = Self;
}

unsafe impl<'a> glib::value::FromValue<'a> for VulkanHandleType {
    type Checker = glib::value::GenericValueTypeChecker<Self>;

    #[inline]
    unsafe fn from_value(value: &'a glib::Value) -> Self {
        skip_assert_initialized!();
        from_glib(glib::gobject_ffi::g_value_get_enum(value.to_glib_none().0))
    }
}

impl ToValue for VulkanHandleType {
    #[inline]
    fn to_value(&self) -> glib::Value {
        let mut value = glib::Value::for_value_type::<Self>();
        unsafe {
            glib::gobject_ffi::g_value_set_enum(value.to_glib_none_mut().0, self.into_glib());
        }
        value
    }

    #[inline]
    fn value_type(&self) -> glib::Type {
        Self::static_type()
    }
}

impl From<VulkanHandleType> for glib::Value {
    #[inline]
    fn from(v: VulkanHandleType) -> Self {
        skip_assert_initialized!();
        ToValue::to_value(&v)
    }
}

#[cfg(feature = "v1_24")]
#[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Clone, Copy)]
#[non_exhaustive]
#[doc(alias = "GstVulkanVideoOperation")]
pub enum VulkanVideoOperation {
    #[doc(alias = "GST_VULKAN_VIDEO_OPERATION_DECODE")]
    Decode,
    #[doc(alias = "GST_VULKAN_VIDEO_OPERATION_ENCODE")]
    Encode,
    #[doc(alias = "GST_VULKAN_VIDEO_OPERATION_UNKNOWN")]
    Unknown,
    #[doc(hidden)]
    __Unknown(i32),
}

#[cfg(feature = "v1_24")]
#[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
#[doc(hidden)]
impl IntoGlib for VulkanVideoOperation {
    type GlibType = ffi::GstVulkanVideoOperation;

    #[inline]
    fn into_glib(self) -> ffi::GstVulkanVideoOperation {
        match self {
            Self::Decode => ffi::GST_VULKAN_VIDEO_OPERATION_DECODE,
            Self::Encode => ffi::GST_VULKAN_VIDEO_OPERATION_ENCODE,
            Self::Unknown => ffi::GST_VULKAN_VIDEO_OPERATION_UNKNOWN,
            Self::__Unknown(value) => value,
        }
    }
}

#[cfg(feature = "v1_24")]
#[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
#[doc(hidden)]
impl FromGlib<ffi::GstVulkanVideoOperation> for VulkanVideoOperation {
    #[inline]
    unsafe fn from_glib(value: ffi::GstVulkanVideoOperation) -> Self {
        skip_assert_initialized!();

        match value {
            ffi::GST_VULKAN_VIDEO_OPERATION_DECODE => Self::Decode,
            ffi::GST_VULKAN_VIDEO_OPERATION_ENCODE => Self::Encode,
            ffi::GST_VULKAN_VIDEO_OPERATION_UNKNOWN => Self::Unknown,
            value => Self::__Unknown(value),
        }
    }
}

#[cfg(feature = "v1_24")]
#[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
impl StaticType for VulkanVideoOperation {
    #[inline]
    #[doc(alias = "gst_vulkan_video_operation_get_type")]
    fn static_type() -> glib::Type {
        unsafe { from_glib(ffi::gst_vulkan_video_operation_get_type()) }
    }
}

#[cfg(feature = "v1_24")]
#[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
impl glib::HasParamSpec for VulkanVideoOperation {
    type ParamSpec = glib::ParamSpecEnum;
    type SetValue = Self;
    type BuilderFn = fn(&str, Self) -> glib::ParamSpecEnumBuilder<Self>;

    fn param_spec_builder() -> Self::BuilderFn {
        Self::ParamSpec::builder_with_default
    }
}

#[cfg(feature = "v1_24")]
#[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
impl glib::value::ValueType for VulkanVideoOperation {
    type Type = Self;
}

#[cfg(feature = "v1_24")]
#[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
unsafe impl<'a> glib::value::FromValue<'a> for VulkanVideoOperation {
    type Checker = glib::value::GenericValueTypeChecker<Self>;

    #[inline]
    unsafe fn from_value(value: &'a glib::Value) -> Self {
        skip_assert_initialized!();
        from_glib(glib::gobject_ffi::g_value_get_enum(value.to_glib_none().0))
    }
}

#[cfg(feature = "v1_24")]
#[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
impl ToValue for VulkanVideoOperation {
    #[inline]
    fn to_value(&self) -> glib::Value {
        let mut value = glib::Value::for_value_type::<Self>();
        unsafe {
            glib::gobject_ffi::g_value_set_enum(value.to_glib_none_mut().0, self.into_glib());
        }
        value
    }

    #[inline]
    fn value_type(&self) -> glib::Type {
        Self::static_type()
    }
}

#[cfg(feature = "v1_24")]
#[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
impl From<VulkanVideoOperation> for glib::Value {
    #[inline]
    fn from(v: VulkanVideoOperation) -> Self {
        skip_assert_initialized!();
        ToValue::to_value(&v)
    }
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Clone, Copy)]
#[non_exhaustive]
#[doc(alias = "GstVulkanWindowError")]
pub enum VulkanWindowError {
    #[doc(alias = "GST_VULKAN_WINDOW_ERROR_FAILED")]
    Failed,
    #[doc(alias = "GST_VULKAN_WINDOW_ERROR_OLD_LIBS")]
    OldLibs,
    #[doc(alias = "GST_VULKAN_WINDOW_ERROR_RESOURCE_UNAVAILABLE")]
    ResourceUnavailable,
    #[doc(hidden)]
    __Unknown(i32),
}

impl VulkanWindowError {
    #[doc(alias = "gst_vulkan_window_error_quark")]
    pub fn quark() -> glib::Quark {
        assert_initialized_main_thread!();
        unsafe { from_glib(ffi::gst_vulkan_window_error_quark()) }
    }
}

#[doc(hidden)]
impl IntoGlib for VulkanWindowError {
    type GlibType = ffi::GstVulkanWindowError;

    #[inline]
    fn into_glib(self) -> ffi::GstVulkanWindowError {
        match self {
            Self::Failed => ffi::GST_VULKAN_WINDOW_ERROR_FAILED,
            Self::OldLibs => ffi::GST_VULKAN_WINDOW_ERROR_OLD_LIBS,
            Self::ResourceUnavailable => ffi::GST_VULKAN_WINDOW_ERROR_RESOURCE_UNAVAILABLE,
            Self::__Unknown(value) => value,
        }
    }
}

#[doc(hidden)]
impl FromGlib<ffi::GstVulkanWindowError> for VulkanWindowError {
    #[inline]
    unsafe fn from_glib(value: ffi::GstVulkanWindowError) -> Self {
        skip_assert_initialized!();

        match value {
            ffi::GST_VULKAN_WINDOW_ERROR_FAILED => Self::Failed,
            ffi::GST_VULKAN_WINDOW_ERROR_OLD_LIBS => Self::OldLibs,
            ffi::GST_VULKAN_WINDOW_ERROR_RESOURCE_UNAVAILABLE => Self::ResourceUnavailable,
            value => Self::__Unknown(value),
        }
    }
}

impl glib::error::ErrorDomain for VulkanWindowError {
    #[inline]
    fn domain() -> glib::Quark {
        skip_assert_initialized!();

        static QUARK: once_cell::sync::Lazy<glib::ffi::GQuark> =
            once_cell::sync::Lazy::new(|| unsafe {
                glib::ffi::g_quark_from_static_string(
                    b"gst-gl-window-error-quark\0".as_ptr() as *const _
                )
            });
        unsafe { from_glib(*QUARK) }
    }

    #[inline]
    fn code(self) -> i32 {
        self.into_glib()
    }

    #[inline]
    #[allow(clippy::match_single_binding)]
    fn from(code: i32) -> Option<Self> {
        skip_assert_initialized!();
        match unsafe { from_glib(code) } {
            Self::__Unknown(_) => Some(Self::Failed),
            value => Some(value),
        }
    }
}

impl StaticType for VulkanWindowError {
    #[inline]
    #[doc(alias = "gst_vulkan_window_error_get_type")]
    fn static_type() -> glib::Type {
        unsafe { from_glib(ffi::gst_vulkan_window_error_get_type()) }
    }
}

impl glib::HasParamSpec for VulkanWindowError {
    type ParamSpec = glib::ParamSpecEnum;
    type SetValue = Self;
    type BuilderFn = fn(&str, Self) -> glib::ParamSpecEnumBuilder<Self>;

    fn param_spec_builder() -> Self::BuilderFn {
        Self::ParamSpec::builder_with_default
    }
}

impl glib::value::ValueType for VulkanWindowError {
    type Type = Self;
}

unsafe impl<'a> glib::value::FromValue<'a> for VulkanWindowError {
    type Checker = glib::value::GenericValueTypeChecker<Self>;

    #[inline]
    unsafe fn from_value(value: &'a glib::Value) -> Self {
        skip_assert_initialized!();
        from_glib(glib::gobject_ffi::g_value_get_enum(value.to_glib_none().0))
    }
}

impl ToValue for VulkanWindowError {
    #[inline]
    fn to_value(&self) -> glib::Value {
        let mut value = glib::Value::for_value_type::<Self>();
        unsafe {
            glib::gobject_ffi::g_value_set_enum(value.to_glib_none_mut().0, self.into_glib());
        }
        value
    }

    #[inline]
    fn value_type(&self) -> glib::Type {
        Self::static_type()
    }
}

impl From<VulkanWindowError> for glib::Value {
    #[inline]
    fn from(v: VulkanWindowError) -> Self {
        skip_assert_initialized!();
        ToValue::to_value(&v)
    }
}
