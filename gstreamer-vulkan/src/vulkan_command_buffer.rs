// Take a look at the license at the top of the repository in the LICENSE file.

use std::fmt;

use glib::translate::*;

gst::mini_object_wrapper!(
    VulkanCommandBuffer,
    VulkanCommandBufferRef,
    ffi::GstVulkanCommandBuffer,
    || ffi::gst_vulkan_command_buffer_get_type()
);

impl fmt::Debug for VulkanCommandBuffer {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        VulkanCommandBufferRef::fmt(self, f)
    }
}

impl fmt::Debug for VulkanCommandBufferRef {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("VulkanCommandBuffer")
            .field("inner", &self.0)
            .finish()

        // .finish_non_exhaustive()
    }
}

impl VulkanCommandBuffer {
    #[doc(alias = "gst_vulkan_command_buffer_new_wrapped")]
    pub fn new_wrapped(
        cmd: vulkan::CommandBuffer,
        level: vulkan::CommandBufferLevel,
    ) -> VulkanCommandBuffer {
        assert_initialized_main_thread!();
        unsafe { from_glib_full(ffi::gst_vulkan_command_buffer_new_wrapped(cmd, level)) }
    }
}

impl VulkanCommandBufferRef {
    pub fn cmd(&self) -> vulkan::CommandBuffer {
        self.0.cmd
    }

    // TODO: pool getter

    pub fn level(&self) -> vulkan::CommandBufferLevel {
        self.0.level
    }
}
