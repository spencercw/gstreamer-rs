// Take a look at the license at the top of the repository in the LICENSE file.

#![cfg_attr(docsrs, feature(doc_cfg))]
#![allow(clippy::missing_safety_doc)]
#![doc = include_str!("../README.md")]

pub use ffi;
pub use glib;
pub use gst;
pub use gst_base;
pub use gst_video;

macro_rules! assert_initialized_main_thread {
    () => {
        if !gst::INITIALIZED.load(std::sync::atomic::Ordering::SeqCst) {
            gst::assert_initialized();
        }
    };
}

macro_rules! skip_assert_initialized {
    () => {};
}

#[allow(
    clippy::match_same_arms,
    clippy::too_many_arguments,
    clippy::type_complexity,
    clippy::unreadable_literal,
    unused_imports
)]
mod auto;
pub use crate::auto::*;

mod buffer_pool_config;
pub use crate::buffer_pool_config::*;
mod caps_features;
pub use crate::caps_features::*;
mod context;
pub use crate::context::*;
mod vulkan_buffer_memory;
pub use crate::vulkan_buffer_memory::*;
mod vulkan_command_buffer;
pub use crate::vulkan_command_buffer::*;
mod vulkan_descriptor_set;
pub use crate::vulkan_descriptor_set::*;
mod vulkan_handle;
pub use crate::vulkan_handle::*;
mod vulkan_image_memory;
pub use crate::vulkan_image_memory::*;
mod vulkan_image_view;
pub use crate::vulkan_image_view::*;
mod vulkan_memory;
pub use crate::vulkan_memory::*;
mod vulkan_swapper;
pub use crate::vulkan_swapper::*;

pub mod functions;
pub use crate::functions::*;

// Re-export all the traits in a prelude module, so that applications
// can always "use gst_vulkan::prelude::*" without getting conflicts
pub mod prelude {
    #[doc(hidden)]
    pub use gst_video::prelude::*;

    pub use crate::{
        auto::traits::*, buffer_pool_config::VulkanBufferPoolConfig, context::ContextVulkanExt,
        vulkan_swapper::VulkanSwapperExtManual,
    };
}

pub mod subclass;
