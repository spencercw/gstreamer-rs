use gst_base::subclass::prelude::*;

use crate::VulkanVideoFilter;

pub trait VulkanVideoFilterImpl: VulkanVideoFilterImplExt + BaseTransformImpl {}

pub trait VulkanVideoFilterImplExt: ObjectSubclass {}

impl<T: VulkanVideoFilterImpl> VulkanVideoFilterImplExt for T {}

unsafe impl<T: VulkanVideoFilterImpl> IsSubclassable<T> for VulkanVideoFilter {
    fn class_init(klass: &mut glib::Class<Self>) {
        <gst_base::BaseTransform as IsSubclassable<T>>::class_init(klass);
    }

    fn instance_init(instance: &mut glib::subclass::InitializingObject<T>) {
        <gst_base::BaseTransform as IsSubclassable<T>>::instance_init(instance)
    }
}
