use glib::prelude::*;
use glib::translate::*;

use crate::VulkanDevice;
use crate::VulkanImageView;

use ffi::GstVulkanImageMemory;
use gst::{Memory, MemoryRef};

use std::fmt;

// gst::mini_object_wrapper!(
//     VulkanImageMemory,
//     VulkanImageMemoryRef,
//     GstVulkanImageMemory
// );

// use crate::{VulkanMemory, VulkanMemoryRef};

gst::memory_object_wrapper!(
    VulkanImageMemory,
    VulkanImageMemoryRef,
    GstVulkanImageMemory,
    |mem: &MemoryRef| { unsafe { from_glib(ffi::gst_is_vulkan_image_memory(mem.as_mut_ptr())) } },
    // VulkanMemory,
    // VulkanMemoryRef,
    Memory,
    MemoryRef
);

impl fmt::Debug for VulkanImageMemory {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        VulkanImageMemoryRef::fmt(self, f)
    }
}

impl fmt::Debug for VulkanImageMemoryRef {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        MemoryRef::fmt(self, f)
    }
}

// impl std::ops::Deref for VulkanImageMemoryRef {
//     type Target = MemoryRef;

//     fn deref(&self) -> &Self::Target {
//         unsafe { &*(&self.0.parent as *const GstMemory).cast::<Self::Target>() }
//     }
// }

// impl std::ops::DerefMut for VulkanImageMemoryRef {
//     fn deref_mut(&mut self) -> &mut Self::Target {
//         unsafe { &mut *(&mut self.0.parent as *mut GstMemory).cast::<Self::Target>() }
//     }
// }

impl VulkanImageMemory {
    #[doc(alias = "gst_vulkan_image_memory_alloc")]
    pub fn alloc(
        device: &impl IsA<VulkanDevice>,
        format: vulkan::Format,
        width: usize,
        height: usize,
        tiling: vulkan::ImageTiling,
        usage: vulkan::ImageUsageFlags,
        mem_prop_flags: vulkan::MemoryPropertyFlags,
    ) -> gst::Memory {
        skip_assert_initialized!();
        Self::init_once();
        unsafe {
            from_glib_full(ffi::gst_vulkan_image_memory_alloc(
                device.as_ref().to_glib_none().0,
                format,
                width,
                height,
                tiling,
                usage,
                mem_prop_flags,
            ))
        }
    }

    #[cfg(feature = "v1_24")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
    #[doc(alias = "gst_vulkan_image_memory_alloc_with_image_info")]
    pub fn alloc_with_image_info(
        device: &impl IsA<VulkanDevice>,
        // _vk_image_mem_new_alloc_with_image_info actually overwrites fields in this struct
        image_info: &mut vulkan::ImageCreateInfo,
        mem_prop_flags: vulkan::MemoryPropertyFlags,
    ) -> gst::Memory {
        skip_assert_initialized!();
        Self::init_once();
        unsafe {
            from_glib_full(ffi::gst_vulkan_image_memory_alloc_with_image_info(
                device.as_ref().to_glib_none().0,
                image_info,
                mem_prop_flags,
            ))
        }
    }

    #[doc(alias = "gst_vulkan_image_memory_init_once")]
    fn init_once() {
        assert_initialized_main_thread!();
        unsafe {
            ffi::gst_vulkan_image_memory_init_once();
        }
    }

    //#[doc(alias = "gst_vulkan_image_memory_wrapped")]
    //pub fn wrapped(device: &impl IsA<VulkanDevice>, image: vulkan::Image, format: vulkan::Format, width: usize, height: usize, tiling: vulkan::ImageTiling, usage: vulkan::ImageUsageFlags, user_data: /*Unimplemented*/Option<Basic: Pointer>) -> gst::Memory {
    //    unsafe { TODO: call ffi:gst_vulkan_image_memory_wrapped() }
    //}
}

impl VulkanImageMemoryRef {
    #[doc(alias = "gst_vulkan_image_memory_add_view")]
    pub fn add_view(&mut self, view: &VulkanImageView) {
        unsafe {
            ffi::gst_vulkan_image_memory_add_view(&mut self.0, view.to_glib_none().0);
        }
    }

    #[doc(alias = "gst_vulkan_image_memory_find_view")]
    pub fn find_view<P: FnMut(&VulkanImageView) -> bool>(
        // TODO: Mut?
        &mut self,
        find_func: P,
    ) -> Option<VulkanImageView> {
        let find_func_data: P = find_func;
        unsafe extern "C" fn find_func_func<P: FnMut(&VulkanImageView) -> bool>(
            view: *mut ffi::GstVulkanImageView,
            user_data: glib::ffi::gpointer,
        ) -> glib::ffi::gboolean {
            let view = from_glib_borrow(view);
            let callback: *mut P = user_data as *const _ as usize as *mut P;
            (*callback)(&view).into_glib()
        }
        let find_func = Some(find_func_func::<P> as _);
        let super_callback0: &P = &find_func_data;
        unsafe {
            from_glib_full(ffi::gst_vulkan_image_memory_find_view(
                &mut self.0,
                find_func,
                super_callback0 as *const _ as usize as *mut _,
            ))
        }
    }

    #[doc(alias = "gst_vulkan_image_memory_get_height")]
    #[doc(alias = "get_height")]
    pub fn height(&self) -> u32 {
        unsafe { ffi::gst_vulkan_image_memory_get_height(mut_override(&self.0)) }
    }

    #[doc(alias = "gst_vulkan_image_memory_get_width")]
    #[doc(alias = "get_width")]
    pub fn width(&self) -> u32 {
        unsafe { ffi::gst_vulkan_image_memory_get_width(mut_override(&self.0)) }
    }

    //#[doc(alias = "gst_vulkan_image_memory_init")]
    //pub fn init(&mut self, allocator: &impl IsA<gst::Allocator>, parent: &gst::Memory, device: &impl IsA<VulkanDevice>, format: vulkan::Format, usage: vulkan::ImageUsageFlags, params: &mut gst::AllocationParams, size: usize, user_data: /*Unimplemented*/Option<Basic: Pointer>) -> bool {
    //    unsafe { TODO: call ffi:gst_vulkan_image_memory_init() }
    //}
}
