use glib::prelude::*;
use glib::translate::*;

use crate::VulkanDevice;

use ffi::GstVulkanMemory;
// use gst::ffi::GstMemory;
use gst::{Memory, MemoryRef};

use std::fmt;

// gst::mini_object_wrapper!(VulkanMemory, VulkanMemoryRef, GstVulkanMemory);
gst::memory_object_wrapper!(
    VulkanMemory,
    VulkanMemoryRef,
    GstVulkanMemory,
    |mem: &MemoryRef| { unsafe { from_glib(ffi::gst_is_vulkan_memory(mem.as_mut_ptr())) } },
    Memory,
    MemoryRef
);

impl fmt::Debug for VulkanMemory {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        VulkanMemoryRef::fmt(self, f)
    }
}

impl fmt::Debug for VulkanMemoryRef {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        MemoryRef::fmt(self, f)
    }
}

// impl std::ops::Deref for VulkanMemoryRef {
//     type Target = MemoryRef;

//     fn deref(&self) -> &Self::Target {
//         unsafe { &*(&self.0.mem as *const GstMemory).cast::<Self::Target>() }
//     }
// }

// impl std::ops::DerefMut for VulkanMemoryRef {
//     fn deref_mut(&mut self) -> &mut Self::Target {
//         unsafe { &mut *(&mut self.0.mem as *mut GstMemory).cast::<Self::Target>() }
//     }
// }

impl VulkanMemory {
    #[doc(alias = "gst_vulkan_memory_alloc")]
    pub fn alloc(
        device: &impl IsA<VulkanDevice>,
        memory_type_index: u32,
        params: &gst::AllocationParams,
        size: usize,
        mem_prop_flags: vulkan::MemoryPropertyFlags,
    ) -> gst::Memory {
        skip_assert_initialized!();
        Self::init_once();
        unsafe {
            from_glib_full(ffi::gst_vulkan_memory_alloc(
                device.as_ref().to_glib_none().0,
                memory_type_index,
                mut_override(params.to_glib_none().0),
                size,
                mem_prop_flags,
            ))
        }
    }

    #[cfg(feature = "v1_24")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
    #[doc(alias = "gst_vulkan_memory_find_memory_type_index_with_requirements")]
    pub fn find_memory_type_index_with_requirements(
        device: &impl IsA<VulkanDevice>,
        req: &vulkan::MemoryRequirements,
        properties: vulkan::MemoryPropertyFlags,
    ) -> Result<u32, glib::BoolError> {
        skip_assert_initialized!();
        let mut type_index = std::mem::MaybeUninit::uninit();
        unsafe {
            glib::result_from_gboolean!(
                ffi::gst_vulkan_memory_find_memory_type_index_with_requirements(
                    device.as_ref().to_glib_none().0,
                    req,
                    properties,
                    type_index.as_mut_ptr(),
                ),
                "Failed to find appropriate memory type index for requirements"
            )
            .map(|()| type_index.assume_init())
        }
    }

    #[doc(alias = "gst_vulkan_memory_heap_flags_to_string")]
    pub fn heap_flags_to_string(prop_bits: vulkan::MemoryHeapFlags) -> glib::GString {
        assert_initialized_main_thread!();
        unsafe { from_glib_full(ffi::gst_vulkan_memory_heap_flags_to_string(prop_bits)) }
    }

    #[doc(alias = "gst_vulkan_memory_init_once")]
    fn init_once() {
        assert_initialized_main_thread!();
        unsafe {
            ffi::gst_vulkan_memory_init_once();
        }
    }

    #[doc(alias = "gst_vulkan_memory_property_flags_to_string")]
    pub fn property_flags_to_string(prop_bits: vulkan::MemoryPropertyFlags) -> glib::GString {
        assert_initialized_main_thread!();
        unsafe { from_glib_full(ffi::gst_vulkan_memory_property_flags_to_string(prop_bits)) }
    }
}
