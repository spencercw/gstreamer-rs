// Take a look at the license at the top of the repository in the LICENSE file.

use std::fmt;

use crate::{VulkanDevice, VulkanImageMemory};
use glib::translate::*;

gst::mini_object_wrapper!(
    VulkanImageView,
    VulkanImageViewRef,
    ffi::GstVulkanImageView,
    || ffi::gst_vulkan_image_view_get_type()
);

impl fmt::Debug for VulkanImageView {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        VulkanImageViewRef::fmt(self, f)
    }
}

impl fmt::Debug for VulkanImageViewRef {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("VulkanImageView")
            .field("device", &unsafe { self.device() })
            .field("image", &unsafe { self.image() })
            .field("view", &self.view())
            .field("create_info", &self.create_info())
            .finish_non_exhaustive()
    }
}

impl VulkanImageView {
    #[doc(alias = "gst_vulkan_image_view_new")]
    // TODO: Mut?
    pub fn new(image: &mut VulkanImageMemory, create_info: &vulkan::ImageViewCreateInfo) -> Self {
        assert_initialized_main_thread!();
        unsafe {
            from_glib_full(ffi::gst_vulkan_image_view_new(
                image.to_glib_none_mut().0,
                create_info,
            ))
        }
    }
}

impl VulkanImageViewRef {
    pub unsafe fn device(&self) -> VulkanDevice {
        // from_glib_borrow(self.0.device)
        from_glib_none(self.0.device)
    }

    pub unsafe fn image(&self) -> VulkanImageMemory {
        // from_glib_borrow(self.0.image)
        from_glib_none(self.0.image)
    }

    pub fn view(&self) -> vulkan::ImageView {
        self.0.view
    }

    pub fn create_info(&self) -> &vulkan::ImageViewCreateInfo {
        &self.0.create_info
    }
}
