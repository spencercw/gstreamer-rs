use crate::VulkanHandle;
use ffi::{GstVulkanHandle, GstVulkanHandleTypedef};
use glib::translate::*;

// TODO: ffi has this as a mut ptr to c_void, while it should apparently be a c_void as-is?
// TODO: Upstream now switched to an alias:
// <alias name="VulkanHandleTypedef" c:type="GstVulkanHandleTypedef">
//   <type name="guint64" c:type="uint64_t"/>
// </alias>
pub type VulkanHandleTypedef = GstVulkanHandleTypedef;
// (was <record name="VulkanHandleTypedef" c:type="GstVulkanHandleTypedef" disguised="1" version="1.18"/> before)
// pub struct VulkanHandleTypedef(GstVulkanHandleTypedef);

// impl<'a> ToGlibPtr<'a, GstVulkanHandleTypedef> for VulkanHandleTypedef {
//     type Storage = &'a Self;

//     fn to_glib_none(&'a self) -> Stash<'a, GstVulkanHandleTypedef, Self> {
//         Stash(self.0, self)
//     }
// }

// impl<'a> ToGlibContainerFromSlice<'a, *const *mut GstVulkanHandle> for &'a VulkanHandle {
//     type Storage = &'a [Self];

//     fn to_glib_none_from_slice(t: Self::Storage) -> (*const *mut GstVulkanHandle, Self::Storage) {
//         skip_assert_initialized!();
//         assert_eq!(
//             std::mem::size_of::<Self>(),
//             std::mem::size_of::<*mut GstVulkanHandle>()
//         );
//         let t_ptrs: &[*mut GstVulkanHandle] = unsafe { std::mem::transmute(t) };
//         (t_ptrs.as_ptr(), t)
//     }

//     fn to_glib_container_from_slice(
//         _t: Self::Storage,
//     ) -> (*const *mut GstVulkanHandle, Self::Storage) {
//         skip_assert_initialized!();
//         unimplemented!("Consumer can't free *const pointer")
//     }

//     fn to_glib_full_from_slice(_t: &[Self]) -> *const *mut GstVulkanHandle {
//         skip_assert_initialized!();
//         unimplemented!("Consumer can't free *const pointer")
//     }
// }

impl<'a> ToGlibContainerFromSlice<'a, *mut *mut GstVulkanHandle> for &'a VulkanHandle {
    type Storage = &'a [Self];

    fn to_glib_none_from_slice(t: Self::Storage) -> (*mut *mut GstVulkanHandle, Self::Storage) {
        skip_assert_initialized!();
        assert_eq!(
            std::mem::size_of::<Self>(),
            std::mem::size_of::<*mut GstVulkanHandle>()
        );
        let t_ptrs: &[*mut GstVulkanHandle] = unsafe { std::mem::transmute(t) };
        (t_ptrs.as_ptr().cast_mut(), t)
    }

    fn to_glib_container_from_slice(
        _t: Self::Storage,
    ) -> (*mut *mut GstVulkanHandle, Self::Storage) {
        skip_assert_initialized!();
        unimplemented!("Consumer can't free *mut pointer")
    }

    fn to_glib_full_from_slice(_t: &[Self]) -> *mut *mut GstVulkanHandle {
        skip_assert_initialized!();
        unimplemented!("Consumer can't free *mut pointer")
    }
}
