#[cfg(feature = "v1_24")]
use glib::translate::*;
use gst::BufferPoolConfigRef;

pub trait VulkanBufferPoolConfig {
    #[cfg(feature = "v1_24")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
    #[doc(alias = "gst_vulkan_buffer_pool_config_set_allocation_params")]
    fn set_allocation_params(
        &mut self,
        usage: vulkan::ImageUsageFlags,
        mem_properties: vulkan::MemoryPropertyFlags,
    );

    #[cfg(feature = "v1_24")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
    #[doc(alias = "gst_vulkan_image_buffer_pool_config_set_allocation_params")]
    fn image_set_allocation_params(
        &mut self,
        usage: vulkan::ImageUsageFlags,
        mem_properties: vulkan::MemoryPropertyFlags,
        initial_layout: vulkan::ImageLayout,
        initial_access: vulkan::AccessFlags2,
    );

    #[cfg(feature = "v1_24")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
    #[doc(alias = "gst_vulkan_image_buffer_pool_config_set_decode_caps")]
    fn image_set_decode_caps(&mut self, caps: &gst::CapsRef);
}

impl VulkanBufferPoolConfig for BufferPoolConfigRef {
    #[cfg(feature = "v1_24")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
    fn set_allocation_params(
        &mut self,
        usage: vulkan::ImageUsageFlags,
        mem_properties: vulkan::MemoryPropertyFlags,
    ) {
        assert_initialized_main_thread!();
        unsafe {
            ffi::gst_vulkan_buffer_pool_config_set_allocation_params(
                self.as_mut_ptr(),
                usage,
                mem_properties,
            );
        }
    }

    #[cfg(feature = "v1_24")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
    fn image_set_allocation_params(
        &mut self,
        usage: vulkan::ImageUsageFlags,
        mem_properties: vulkan::MemoryPropertyFlags,
        initial_layout: vulkan::ImageLayout,
        initial_access: vulkan::AccessFlags2,
    ) {
        assert_initialized_main_thread!();
        unsafe {
            ffi::gst_vulkan_image_buffer_pool_config_set_allocation_params(
                self.as_mut_ptr(),
                usage,
                mem_properties,
                initial_layout,
                initial_access.as_raw(),
            );
        }
    }

    #[cfg(feature = "v1_24")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_24")))]
    fn image_set_decode_caps(&mut self, caps: &gst::CapsRef) {
        assert_initialized_main_thread!();
        unsafe {
            ffi::gst_vulkan_image_buffer_pool_config_set_decode_caps(
                self.as_mut_ptr(),
                mut_override(caps.as_ptr()),
            );
        }
    }
}
