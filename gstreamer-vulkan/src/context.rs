use gst::ContextRef;

use glib::{prelude::*, translate::*};

use crate::{VulkanDevice, VulkanDisplay, VulkanInstance, VulkanQueue};

pub trait ContextVulkanExt {
    #[doc(alias = "get_vulkan_device")]
    #[doc(alias = "gst_context_get_vulkan_device")]
    fn vulkan_device(&self) -> Option<VulkanDevice>;
    #[doc(alias = "get_vulkan_display")]
    #[doc(alias = "gst_context_get_vulkan_display")]
    fn vulkan_display(&self) -> Option<VulkanDisplay>;
    #[doc(alias = "get_vulkan_instance")]
    #[doc(alias = "gst_context_get_vulkan_instance")]
    fn vulkan_instance(&self) -> Option<VulkanInstance>;
    #[doc(alias = "get_vulkan_queue")]
    #[doc(alias = "gst_context_get_vulkan_queue")]
    fn vulkan_queue(&self) -> Option<VulkanQueue>;
    #[doc(alias = "gst_context_set_vulkan_device")]
    fn set_vulkan_device(&self, device: Option<&impl IsA<VulkanDevice>>);
    #[doc(alias = "gst_context_set_vulkan_display")]
    fn set_vulkan_display(&self, display: Option<&impl IsA<VulkanDisplay>>);
    #[doc(alias = "gst_context_set_vulkan_instance")]
    fn set_vulkan_instance(&self, instance: Option<&impl IsA<VulkanInstance>>);
    #[doc(alias = "gst_context_set_vulkan_queue")]
    fn set_vulkan_queue(&self, queue: Option<&impl IsA<VulkanQueue>>);
}

impl ContextVulkanExt for ContextRef {
    fn vulkan_device(&self) -> Option<VulkanDevice> {
        assert_initialized_main_thread!();
        unsafe {
            let mut device = std::ptr::null_mut();
            let ret = from_glib(ffi::gst_context_get_vulkan_device(
                self.as_mut_ptr(),
                &mut device,
            ));
            if ret {
                Some(from_glib_full(device))
            } else {
                None
            }
        }
    }

    fn vulkan_display(&self) -> Option<VulkanDisplay> {
        assert_initialized_main_thread!();
        unsafe {
            let mut display = std::ptr::null_mut();
            let ret = from_glib(ffi::gst_context_get_vulkan_display(
                self.as_mut_ptr(),
                &mut display,
            ));
            if ret {
                Some(from_glib_full(display))
            } else {
                None
            }
        }
    }

    fn vulkan_instance(&self) -> Option<VulkanInstance> {
        assert_initialized_main_thread!();
        unsafe {
            let mut instance = std::ptr::null_mut();
            let ret = from_glib(ffi::gst_context_get_vulkan_instance(
                self.as_mut_ptr(),
                &mut instance,
            ));
            if ret {
                Some(from_glib_full(instance))
            } else {
                None
            }
        }
    }

    fn vulkan_queue(&self) -> Option<VulkanQueue> {
        assert_initialized_main_thread!();
        unsafe {
            let mut queue = std::ptr::null_mut();
            let ret = from_glib(ffi::gst_context_get_vulkan_queue(
                self.as_mut_ptr(),
                &mut queue,
            ));
            if ret {
                Some(from_glib_full(queue))
            } else {
                None
            }
        }
    }

    fn set_vulkan_device(&self, device: Option<&impl IsA<VulkanDevice>>) {
        assert_initialized_main_thread!();
        unsafe {
            ffi::gst_context_set_vulkan_device(
                self.as_mut_ptr(),
                device.map(|p| p.as_ref()).to_glib_none().0,
            );
        }
    }

    fn set_vulkan_display(&self, display: Option<&impl IsA<VulkanDisplay>>) {
        assert_initialized_main_thread!();
        unsafe {
            ffi::gst_context_set_vulkan_display(
                self.as_mut_ptr(),
                display.map(|p| p.as_ref()).to_glib_none().0,
            );
        }
    }

    fn set_vulkan_instance(&self, instance: Option<&impl IsA<VulkanInstance>>) {
        assert_initialized_main_thread!();
        unsafe {
            ffi::gst_context_set_vulkan_instance(
                self.as_mut_ptr(),
                instance.map(|p| p.as_ref()).to_glib_none().0,
            );
        }
    }

    fn set_vulkan_queue(&self, queue: Option<&impl IsA<VulkanQueue>>) {
        assert_initialized_main_thread!();
        unsafe {
            ffi::gst_context_set_vulkan_queue(
                self.as_mut_ptr(),
                queue.map(|p| p.as_ref()).to_glib_none().0,
            );
        }
    }
}
