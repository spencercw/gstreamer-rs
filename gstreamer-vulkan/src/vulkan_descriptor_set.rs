// Take a look at the license at the top of the repository in the LICENSE file.

use std::fmt;

use crate::{VulkanDescriptorCache, VulkanDescriptorPool, VulkanHandle};
use glib::{prelude::*, translate::*};

gst::mini_object_wrapper!(
    VulkanDescriptorSet,
    VulkanDescriptorSetRef,
    ffi::GstVulkanDescriptorSet,
    || ffi::gst_vulkan_descriptor_set_get_type()
);

impl fmt::Debug for VulkanDescriptorSet {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        VulkanDescriptorSetRef::fmt(self, f)
    }
}

impl fmt::Debug for VulkanDescriptorSetRef {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("VulkanDescriptorSet")
            .field("set", &self.set())
            .field("set", &self.set())
            .field("pool", &unsafe { self.pool() })
            .field("cache", &unsafe { self.cache() })
            .field("layouts", &unsafe { self.layouts() })
            .finish_non_exhaustive()
    }
}

impl VulkanDescriptorSet {
    #[doc(alias = "gst_vulkan_descriptor_set_new_wrapped")]
    pub fn new_wrapped(
        pool: &impl IsA<VulkanDescriptorPool>,
        set: vulkan::DescriptorSet,
        layouts: &[&VulkanHandle],
    ) -> VulkanDescriptorSet {
        skip_assert_initialized!();
        let n_layouts = layouts.len() as _;
        unsafe {
            from_glib_full(ffi::gst_vulkan_descriptor_set_new_wrapped(
                pool.as_ref().to_glib_none().0,
                set,
                n_layouts,
                layouts.to_glib_none().0,
            ))
        }
    }
}

impl VulkanDescriptorSetRef {
    pub fn set(&self) -> vulkan::DescriptorSet {
        self.0.set
    }

    pub unsafe fn pool(&self) -> VulkanDescriptorPool {
        // from_glib_borrow(self.0.pool)
        from_glib_none(self.0.pool)
    }

    pub unsafe fn cache(&self) -> VulkanDescriptorCache {
        // from_glib_borrow(self.0.cache)
        from_glib_none(self.0.cache)
    }

    pub unsafe fn layouts(&self) -> &[*mut ffi::GstVulkanHandle] {
        let layouts = std::slice::from_raw_parts(self.0.layouts, self.0.n_layouts as usize);
        // from_glib_none(layouts)
        layouts
    }
}
