// Copyright (C) 2021 Marijn Suijten <marijns95@gmail.com>
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crate::VulkanDisplayWayland;
use glib::ffi::gpointer;
use glib::translate::*;
use libc::uintptr_t;

impl VulkanDisplayWayland {
    #[doc(alias = "gst_vulkan_display_wayland_new_with_display")]
    pub unsafe fn with_display(
        display: uintptr_t,
    ) -> Result<VulkanDisplayWayland, glib::error::BoolError> {
        from_glib_full::<_, Option<VulkanDisplayWayland>>(
            ffi::gst_vulkan_display_wayland_new_with_display(display as gpointer),
        )
        .ok_or_else(|| glib::bool_error!("Failed to create new Wayland Vulkan display"))
    }
}
