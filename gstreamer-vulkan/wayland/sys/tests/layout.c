// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// from gst-gir-files (https://gitlab.freedesktop.org/gstreamer/gir-files-rs.git)
// DO NOT EDIT

#include "manual.h"
#include <stdalign.h>
#include <stdio.h>

int main() {
    printf("%s;%zu;%zu\n", "GstVulkanDisplayWayland", sizeof(GstVulkanDisplayWayland), alignof(GstVulkanDisplayWayland));
    printf("%s;%zu;%zu\n", "GstVulkanDisplayWaylandClass", sizeof(GstVulkanDisplayWaylandClass), alignof(GstVulkanDisplayWaylandClass));
    return 0;
}
