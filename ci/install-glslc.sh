set -e

apt -y install cmake

pushd .

cd ..
git clone --depth=1 https://github.com/google/shaderc
cd shaderc
./utils/git-sync-deps
cmake -GNinja -Bbuild -DCMAKE_BUILD_TYPE=Release
cmake --build build
cmake --install build

popd
