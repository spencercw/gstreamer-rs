// This example demonstrates how to output Vulkan textures, within a
// Wayland/XCB context provided by the application, and render those
// textures in the Vulkan application.

// {videotestsrc} - { vulkanupload } - { filter }? - { appsink }

use std::{
    ffi::{CStr, CString},
    sync,
};

use anyhow::Result;
use ash::vk;
use derive_more::{Display, Error};
use glib::translate::*;
use gst::element_error;
use gst_vulkan::prelude::*;
use raw_window_handle::{HasRawDisplayHandle as _, HasRawWindowHandle as _};

#[derive(Debug, Display, Error)]
#[display(fmt = "Received error from {src}: {error} (debug: {debug:?})")]
struct ErrorMessage {
    src: glib::GString,
    error: glib::Error,
    debug: Option<glib::GString>,
}

#[derive(Debug)]
enum Message {
    Sample(gst::Sample),
    BusEvent,
}

pub(crate) struct App {
    pipeline: gst::Pipeline,
    appsink: gst_app::AppSink,
    vulkanupload: gst::Element,
    bus: gst::Bus,
    event_loop: winit::event_loop::EventLoop<Message>,
    window: winit::window::Window,

    pdevice: vk::PhysicalDevice,
    entry: ash::Entry,
    instance: ash::Instance,
    device: ash::Device,
    gst_instance: gst_vulkan::VulkanInstance,
    gst_device: gst_vulkan::VulkanDevice,
    gst_queue: gst_vulkan::VulkanQueue,
}

impl App {
    pub(crate) fn new(vk_element: Option<&gst::Element>) -> Result<App> {
        gst::init()?;

        let (pipeline, appsink, vulkanupload) = App::create_pipeline(vk_element)?;
        let bus = pipeline
            .bus()
            .expect("Pipeline without bus. Shouldn't happen!");

        let event_loop = winit::event_loop::EventLoopBuilder::with_user_event().build()?;
        // TODO: Postpone this for Android?
        let window = winit::window::WindowBuilder::new()
            .with_title("Vulkan rendering")
            .build(&event_loop)
            .expect("Failed to build window");

        // Possible ways to share this instance context:
        // - Set it on an Element that returns it to peers in a context query (vulkanupload);
        // - Provide it in reply to a Message::NeedsContext.
        //
        // Device:
        // - Reply it in the create-device signal;
        // - Set it on every Element?
        // - (Elements DO NOT reply a local device context in queries!);
        //
        // Of course we can also always leave the pipeline to create it "somewhere", and extract it
        // later. But it might lack necessary extensions.

        // Create instance, device and queue via GStreamer, as there are currently no new_wrapped() creators for them

        let gst_instance = gst_vulkan::VulkanInstance::new();
        gst_instance.fill_info().unwrap();
        assert!(gst_instance.enable_layer("VK_LAYER_KHRONOS_validation"));
        assert!(gst_instance
            .enable_extension(ash::extensions::ext::DebugUtils::name().to_str().unwrap()));
        for &ext in
            ash_window::enumerate_required_extensions(event_loop.raw_display_handle()).unwrap()
        {
            let ext = unsafe { CStr::from_ptr(ext.cast()) }
                .to_str()
                .expect("Vulkan strings must be UTF-8");
            assert!(gst_instance.enable_extension(ext));
            // assert!(gst_instance.is_extension_enabled(ext));
        }
        gst_instance.open().unwrap();
        dbg!(gst_instance.version());

        static mut GST_INSTANCE: Option<gst_vulkan::VulkanInstance> = None;
        unsafe { GST_INSTANCE = Some(gst_instance.clone()) };

        let entry = unsafe {
            unsafe extern "system" fn get_instance_proc_addr(
                instance: vk::Instance,
                name: *const std::ffi::c_char,
            ) -> vk::PFN_vkVoidFunction {
                // (*gst_instance.as_ptr()).instance,
                std::mem::transmute(gst_vulkan::ffi::gst_vulkan_instance_get_proc_address(
                    GST_INSTANCE.as_ref().unwrap().to_glib_none().0,
                    name,
                ))
            }

            // TODO: This probably shouldn't be loaded from an instance!
            ash::Entry::from_static_fn(ash::vk::StaticFn {
                get_instance_proc_addr,
            })
        };

        let instance = unsafe {
            let instance = (*gst_instance.as_ptr()).instance;
            // ash::Instance::load_with(
            //     |name| unsafe {
            //         gst_vulkan::ffi::gst_vulkan_instance_get_proc_address(
            //             gst_instance.to_glib_none().0,
            //             name.as_ptr(),
            //         )
            //     },
            //     (*gst_instance.as_ptr()).instance,
            // )

            ash::Instance::load(entry.static_fn(), instance)
        };

        let instance_context =
            gst::Context::new(gst_vulkan::VULKAN_INSTANCE_CONTEXT_TYPE_STR, true);
        instance_context.set_vulkan_instance(Some(&gst_instance));
        vulkanupload.set_context(&instance_context);

        // let gst_display: gst_vulkan::VulkanDisplay = 'have_display: {
        //     #[cfg(feature = "gst-vulkan-xcb")]
        //     {
        //         if let Ok(vk_display) =
        //             gst_vulkan_xcb::VulkanDisplayXCB::new(None /* Some(":0") */)
        //         {
        //             break 'have_display vk_display.upcast();
        //         }
        //     }

        //     #[cfg(feature = "gst-vulkan-wayland")]
        //     {
        //         if let Ok(vk_display) =
        //             gst_vulkan_wayland::VulkanDisplayWayland::new(None /* Some("wayland-1") */)
        //         {
        //             break 'have_display vk_display.upcast();
        //         }
        //     }

        //     panic!()
        // };

        let gst_device = gst_instance.create_device()?;
        // gst_device.open().unwrap();
        let pdevice = dbg!(gst_device.physical_device());

        let device = unsafe {
            let device = (*gst_device.as_ptr()).device;
            dbg!(device);
            // extern "system" fn get_device_proc_addr(
            //     _device: vk::Device,
            //     _p_name: *const std::ffi::c_char,
            // ) -> vk::PFN_vkVoidFunction {
            // }

            // ash::Device::load(&vk::InstanceFnV1_0 {
            //     get_device_proc_addr,
            //     ..Default::default()
            // }, device)
            ash::Device::load(instance.fp_v1_0(), device)
            // ash::Device::load_with(
            //     |name| unsafe {
            //         gst_vulkan::ffi::gst_vulkan_device_get_proc_address(
            //             gst_device.to_glib_none(),
            //             name.as_ptr(),
            //         )
            //     },
            //     unsafe { *gst_device.as_ptr() }.device,
            // )
        };

        // let device_context = gst::Context::new(gst_vulkan::VULKAN_DEVICE_CONTEXT_TYPE_STR, true);
        // device_context.set_vulkan_device(Some(&gst_device));
        // vulkanupload.set_context(&device_context);
        // appsink.set_context(&device_context);
        let gst_device3 = gst_device.clone();
        gst_instance.connect_create_device(move |_instance| gst_device3.clone());

        // let gst_queue = gst_vulkan::VulkanQueue::new();
        let gst_queue = gst_device.queue(0, 0);

        // let gst_queue2 = gst_queue.clone();
        // let gst_instance2 = gst_instance.clone();
        // let gst_device2 = gst_device.clone();

        let event_proxy = sync::Mutex::new(event_loop.create_proxy());

        #[allow(clippy::single_match)]
        bus.set_sync_handler(move |_, msg| {
            // match msg.view() {
            //     gst::MessageView::NeedContext(ctxt) => {
            //         let context_type = ctxt.context_type();
            //         dbg!(context_type);
            //         // TODO: Strip off _STR suffix in bindings
            //         if context_type == *gst_vulkan::VULKAN_INSTANCE_CONTEXT_TYPE_STR {
            //             if let Some(el) =
            //                 msg.src().map(|s| s.downcast_ref::<gst::Element>().unwrap())
            //             {
            //                 let context = gst::Context::new(context_type, true);
            //                 context.set_vulkan_instance(Some(&gst_instance2));
            //                 el.set_context(&context);
            //             }
            //         }
            //         if context_type == *gst_vulkan::VULKAN_DEVICE_CONTEXT_TYPE_STR {
            //             if let Some(el) =
            //                 msg.src().map(|s| s.downcast_ref::<gst::Element>().unwrap())
            //             {
            //                 let context = gst::Context::new(context_type, true);
            //                 context.set_vulkan_device(Some(&gst_device2));
            //                 el.set_context(&context);
            //             }
            //         }
            //         if context_type == *gst_vulkan::VULKAN_QUEUE_CONTEXT_TYPE_STR {
            //             if let Some(el) =
            //                 msg.src().map(|s| s.downcast_ref::<gst::Element>().unwrap())
            //             {
            //                 let context = gst::Context::new(context_type, true);
            //                 context.set_vulkan_queue(Some(&gst_queue2));
            //                 el.set_context(&context);
            //             }
            //         }
            //         if context_type == *gst_vulkan::VULKAN_DISPLAY_CONTEXT_TYPE_STR {
            //             if let Some(el) =
            //                 msg.src().map(|s| s.downcast_ref::<gst::Element>().unwrap())
            //             {
            //                 let context = gst::Context::new(context_type, true);
            //                 context.set_vulkan_display(Some(&gst_display));
            //                 el.set_context(&context);
            //             }
            //         }
            //     }
            //     _ => (),
            // }

            if let Err(e) = event_proxy.lock().unwrap().send_event(Message::BusEvent) {
                eprintln!("Failed to send BusEvent to event proxy: {}", e)
            }

            gst::BusSyncReply::Pass
        });

        Ok(App {
            pipeline,
            appsink,
            vulkanupload,
            bus,
            event_loop,
            window,
            pdevice,
            entry,
            instance,
            device,
            gst_instance,
            gst_device,
            gst_queue,
        })
    }

    fn setup(&self, event_loop: &winit::event_loop::EventLoop<Message>) -> Result<()> {
        let event_proxy = event_loop.create_proxy();
        self.appsink.set_callbacks(
            gst_app::AppSinkCallbacks::builder()
                .new_sample(move |appsink| {
                    let sample = appsink.pull_sample().map_err(|_| gst::FlowError::Eos)?;

                    {
                        let _buffer = sample.buffer().ok_or_else(|| {
                            element_error!(
                                appsink,
                                gst::ResourceError::Failed,
                                ("Failed to get buffer from appsink")
                            );

                            gst::FlowError::Error
                        })?;

                        let _info = sample
                            .caps()
                            .and_then(|caps| gst_video::VideoInfo::from_caps(caps).ok())
                            .ok_or_else(|| {
                                element_error!(
                                    appsink,
                                    gst::ResourceError::Failed,
                                    ("Failed to get video info from sample")
                                );

                                gst::FlowError::Error
                            })?;
                    }

                    event_proxy
                        .send_event(Message::Sample(sample))
                        .map(|()| gst::FlowSuccess::Ok)
                        .map_err(|e| {
                            element_error!(
                                appsink,
                                gst::ResourceError::Failed,
                                ("Failed to send sample to event loop: {}", e)
                            );

                            gst::FlowError::Error
                        })
                })
                .build(),
        );

        self.pipeline.set_state(gst::State::Playing)?;

        Ok(())
    }

    fn create_pipeline(
        vk_element: Option<&gst::Element>,
    ) -> Result<(gst::Pipeline, gst_app::AppSink, gst::Element)> {
        let pipeline = gst::Pipeline::default();
        let src = gst::ElementFactory::make("videotestsrc").build()?;

        let caps = gst_video::VideoCapsBuilder::new()
            .features([gst_vulkan::CAPS_FEATURE_MEMORY_VULKAN_IMAGE])
            // We will select a format when the swapchain is created
            .format_list([gst_video::VideoFormat::Bgra, gst_video::VideoFormat::Rgba])
            // .format_list(gst_vulkan::VULKAN_SWAPPER_VIDEO_FORMATS)
            .field("texture-target", &"2D")
            .build();

        let appsink = gst_app::AppSink::builder()
            .enable_last_sample(true)
            .max_buffers(1)
            .caps(&caps)
            .build();
        // appsink.set_property("emit-signals", &false);

        // if let Some(vk_element) = vk_element {
        let vulkanupload = gst::ElementFactory::make("vulkanupload").build()?;

        pipeline.add_many(&[&src, &vulkanupload, appsink.upcast_ref()])?;
        src.link(&vulkanupload)?;

        if let Some(vk_element) = vk_element {
            pipeline.add(vk_element)?;

            vulkanupload.link(vk_element)?;
            vk_element.link(&appsink)?;
        } else {
            vulkanupload.link(&appsink)?;
        }

        Ok((pipeline, appsink, vulkanupload))
    }

    fn handle_messages(bus: &gst::Bus) -> Result<()> {
        use gst::MessageView;

        for msg in bus.iter() {
            match msg.view() {
                MessageView::Eos(..) => break,
                MessageView::Error(err) => {
                    return Err(ErrorMessage {
                        src: msg
                            .src()
                            .map(|s| s.path_string())
                            .unwrap_or_else(|| glib::GString::from("UNKNOWN")),
                        error: err.error(),
                        debug: err.debug(),
                    }
                    .into());
                }
                _ => (),
            }
        }

        Ok(())
    }
}

pub(crate) fn main_loop(app: App) -> Result<()> {
    app.setup(&app.event_loop)?;

    // let mut curr_frame: Option<gst_video::VideoFrame<gst_video::video_frame::Readable>> = None;
    let mut curr_frame = None;

    let App {
        pipeline,
        appsink,
        vulkanupload,
        bus,
        event_loop,
        window,
        pdevice,
        entry,
        instance,
        device,
        gst_instance,
        gst_device,
        mut gst_queue,
    } = app;

    let mut q = gst::query::Context::new(gst_vulkan::VULKAN_QUEUE_CONTEXT_TYPE_STR);
    assert!(pipeline.query(&mut q));
    let context = q.context().unwrap();
    let gst_queue = context.vulkan_queue().unwrap();
    // TODO: Getting the same queue here gives us a different object! (That breaks the submit lock below)
    // let gst_queue2 = gst_device.queue(0, 0);
    // dbg!(unsafe { *gst_queue2.as_ptr() });
    // dbg!(unsafe { *gst_queue.as_ptr() });
    // assert_eq!(gst_queue.device(), gst_queue2.device());
    // assert_eq!(gst_queue, gst_queue2);

    let swcl = ash::extensions::khr::Swapchain::new(&instance, &device);
    let surfl = ash::extensions::khr::Surface::new(&entry, &instance);
    let debug = ash::extensions::ext::DebugUtils::new(&entry, &instance);

    let queues = unsafe { instance.get_physical_device_queue_family_properties(pdevice) };
    let (transfer_queue_idx, _) = queues
        .iter()
        .enumerate()
        .find(|(i, props)| props.queue_flags.contains(vk::QueueFlags::TRANSFER))
        .unwrap_or((0, &queues[0]));
    let transfer_queue = unsafe { device.get_device_queue(transfer_queue_idx as u32, 0) };

    assert_eq!(
        unsafe { *gst_queue.as_ptr() }.index,
        transfer_queue_idx as u32
    );
    assert_eq!(unsafe { *gst_queue.as_ptr() }.queue, transfer_queue);

    let transfer_cmd_pool = unsafe {
        device.create_command_pool(
            &vk::CommandPoolCreateInfo::builder()
                .flags(vk::CommandPoolCreateFlags::empty())
                .queue_family_index(transfer_queue_idx as u32),
            None,
        )
    }
    .unwrap();

    struct State {
        surface: vk::SurfaceKHR,
        swapchain: vk::SwapchainKHR,
        images: Vec<vk::Image>,
        // Temp hack because the images are UNDEFINED at first
        image_layouts: Vec<vk::ImageLayout>,
    }

    let mut state = None;

    let getters: &[(_, Box<dyn Fn(&gst::ContextRef) + 'static>)] = &[
        (
            gst_vulkan::VULKAN_DISPLAY_CONTEXT_TYPE_STR,
            Box::new(|c: &gst::ContextRef| {
                dbg!(c.vulkan_display());
            }),
        ),
        (
            gst_vulkan::VULKAN_INSTANCE_CONTEXT_TYPE_STR,
            Box::new(|c: &gst::ContextRef| {
                dbg!(c.vulkan_instance());
            }),
        ),
        (
            gst_vulkan::VULKAN_DEVICE_CONTEXT_TYPE_STR,
            Box::new(|c: &gst::ContextRef| {
                let d = dbg!(c.vulkan_device()).unwrap();

                #[cfg(feature = "gst-vulkan/v1_24")]
                {
                    let pool = gst_vulkan::VulkanImageBufferPool::new(&d);
                    let mut config = pool.config();
                    config.set_allocation_params(
                        ImageUsageFlags::TRANSFER_DST,
                        MemoryPropertyFlags::DEVICE_LOCAL,
                    );
                    dbg!(config);
                } // let p = gst_vulkan::VulkanDescriptorPool::new_wrapped(&d, todo!(), 100);
                  // gst_vulkan::VulkanDescriptorSet::new_wrapped(&p, todo!(), &[]);
            }),
        ),
        (
            gst_vulkan::VULKAN_QUEUE_CONTEXT_TYPE_STR,
            Box::new(|c: &gst::ContextRef| {
                let q = dbg!(c.vulkan_queue()).unwrap();
                let pool = q.create_command_pool().unwrap();
                let b = pool.create().unwrap();
                dbg!(b);
            }),
        ),
    ];

    // for (name, getter) in getters {
    //     let mut q = gst::query::Context::new(name);
    //     if dbg!(pipeline.query(&mut q)) {
    //         dbg!(&q);
    //         let context = q.context().unwrap();
    //         getter(context);
    //     }
    // }

    Ok(event_loop.run(move |event, window_target| {
        window_target.set_control_flow(winit::event_loop::ControlFlow::Wait);

        let mut needs_redraw = false;
        match event {
            winit::event::Event::LoopExiting => {
                pipeline.send_event(gst::event::Eos::new());
                pipeline.set_state(gst::State::Null).unwrap();
            }
            winit::event::Event::Suspended => {
                let State {
                    surface,
                    swapchain,
                    images,
                    image_layouts,
                } = state.take().expect("Suspended before Resumed");
                unsafe { surfl.destroy_surface(surface, None) };
            }
            winit::event::Event::Resumed => {
                let surface = unsafe {
                    ash_window::create_surface(
                        &entry,
                        &instance,
                        window.raw_display_handle(),
                        window.raw_window_handle(),
                        None,
                    )
                }
                .unwrap();
                dbg!(surface);

                let caps =
                    unsafe { surfl.get_physical_device_surface_capabilities(pdevice, surface) }
                        .unwrap();

                // TODO: If this is adequate we could provide our images on the appsink pad?
                dbg!(caps);

                let WINDOW_SIZE = vk::Extent2D {
                    width: u32::MAX,
                    height: u32::MAX,
                };
                let window_size = if caps.current_extent == WINDOW_SIZE {
                    let winit::dpi::PhysicalSize::<u32> { width, height } = window.inner_size();
                    vk::Extent2D { width, height }
                } else {
                    caps.current_extent
                };
                dbg!(window_size);

                let formats =
                    unsafe { surfl.get_physical_device_surface_formats(pdevice, surface) }.unwrap();
                dbg!(&formats);
                let format = formats
                    // TODO: Extract from video / negoatiated caps format!
                    .into_iter()
                    .find(|f| {
                        matches!(
                            f.format,
                            vk::Format::R8G8B8A8_UNORM | vk::Format::B8G8R8A8_UNORM
                        ) && f.color_space == vk::ColorSpaceKHR::SRGB_NONLINEAR
                    })
                    .expect("Basic surface format");
                let vk::SurfaceFormatKHR {
                    format,
                    color_space,
                } = format;

                {
                    // Update pipeline with possible caps and reconfigure
                    // TODO: This is a copy from the constructor above. Can we do this smarter?
                    let caps = gst_video::VideoCapsBuilder::new()
                        .features([gst_vulkan::CAPS_FEATURE_MEMORY_VULKAN_IMAGE])
                        .format(match format {
                            vk::Format::R8G8B8A8_UNORM => gst_video::VideoFormat::Rgba,
                            vk::Format::B8G8R8A8_UNORM => gst_video::VideoFormat::Bgra,
                            _ => todo!(),
                        })
                        .field("texture-target", &"2D")
                        // TODO: appsink doesn't seem to propagate this to videotestsrc!
                        // .width(window_size.width as i32)
                        // .height(window_size.height as i32)
                        .build();
                    appsink.set_caps(Some(&caps));
                    pipeline.send_event(gst::event::Reconfigure::new());
                }

                let transfer_queue_idx = [transfer_queue_idx as u32];

                let info = vk::SwapchainCreateInfoKHR {
                    surface,
                    image_color_space: color_space,
                    image_format: format,
                    min_image_count: caps.min_image_count,
                    image_extent: window_size,
                    flags: vk::SwapchainCreateFlagsKHR::empty(),
                    image_array_layers: 1,
                    image_usage: vk::ImageUsageFlags::TRANSFER_DST,
                    image_sharing_mode: vk::SharingMode::EXCLUSIVE,
                    queue_family_index_count: transfer_queue_idx.len() as u32,
                    p_queue_family_indices: transfer_queue_idx.as_ptr(),
                    pre_transform: vk::SurfaceTransformFlagsKHR::IDENTITY,
                    composite_alpha: vk::CompositeAlphaFlagsKHR::OPAQUE,
                    present_mode: vk::PresentModeKHR::MAILBOX,
                    clipped: vk::FALSE,
                    old_swapchain: vk::SwapchainKHR::null(),
                    ..Default::default()
                };

                let swapchain = unsafe { swcl.create_swapchain(&info, None) }.unwrap();

                let images = unsafe { swcl.get_swapchain_images(swapchain) }.unwrap();
                let image_layouts = vec![vk::ImageLayout::UNDEFINED; images.len()];

                for (i, image) in images.iter().enumerate() {
                    unsafe {
                        debug.set_debug_utils_object_name(
                            device.handle(),
                            &vk::DebugUtilsObjectNameInfoEXT::builder()
                                .object_handle(vk::Handle::as_raw(*image))
                                .object_type(<vk::Image as vk::Handle>::TYPE)
                                .object_name(
                                    &CString::new(format!("Swapchain image {i}")).unwrap(),
                                ),
                        )
                    }
                    .unwrap()
                }

                let prev = state.replace(State {
                    surface,
                    swapchain,
                    images,
                    image_layouts,
                });
                assert!(prev.is_none());

                pipeline.set_state(gst::State::Playing).unwrap();
                // pipeline.send_event(gst::event::Play)
            }
            winit::event::Event::WindowEvent { event, .. } => match event {
                winit::event::WindowEvent::CloseRequested
                | winit::event::WindowEvent::KeyboardInput {
                    event:
                        winit::event::KeyEvent {
                            state: winit::event::ElementState::Released,
                            logical_key:
                                winit::keyboard::Key::Named(winit::keyboard::NamedKey::Escape),
                            ..
                        },
                    ..
                } => window_target.exit(),
                winit::event::WindowEvent::Resized(physical_size) => {
                    println!("TODO: Resize to {physical_size:?}");
                    // windowed_context.resize(physical_size);
                    // gl.resize(physical_size);
                }
                winit::event::WindowEvent::RedrawRequested => needs_redraw = true,
                _ => (),
            },
            // Receive a frame
            winit::event::Event::UserEvent(Message::Sample(sample)) => {
                let buffer = sample.buffer_owned().unwrap();
                // let info = sample
                //     .caps()
                //     .and_then(|caps| gst_video::VideoInfo::from_caps(caps).ok())
                //     .unwrap();
                // dbg!(buffer.iter_meta().collect::<Vec<_>>());
                // dbg!(&info);

                for memory in buffer.iter_memories_owned() {
                    // curr_frame = memory.into_mapped_memory_readable().ok();
                    // dbg!(&b);
                    // dbg!(b.value_type());
                    // dbg!(b.downcast_memory_ref::<gst_vulkan::VulkanMemory>());
                    let image = memory
                        .downcast_memory::<gst_vulkan::VulkanImageMemory>()
                        .unwrap();
                    // TODO: Don't overwrite frames!
                    curr_frame = Some(image);
                    needs_redraw = true;
                }
            }
            // Handle all pending messages when we are awaken by set_sync_handler
            winit::event::Event::UserEvent(Message::BusEvent) => {
                App::handle_messages(&bus).unwrap();
            }
            _ => (),
        }

        if needs_redraw {
            if let Some(frame) = curr_frame.take() {
                // let frame = frame.memory();
                // let frame = frame
                //     .downcast_memory_ref::<gst_vulkan::VulkanImageMemory>()
                //     .unwrap();
                if let Some(state) = &mut state {
                    let acquire_sema = unsafe {
                        // .flags(vk::SemaphoreCreateFlags::empty()),
                        device.create_semaphore(&vk::SemaphoreCreateInfo::builder(), None)
                    }
                    .unwrap();
                    let present_sema = unsafe {
                        // .flags(vk::SemaphoreCreateFlags::empty()),
                        device.create_semaphore(&vk::SemaphoreCreateInfo::builder(), None)
                    }
                    .unwrap();
                    let (present_idx, optimal) = unsafe {
                        swcl.acquire_next_image(
                            state.swapchain,
                            u64::MAX,
                            acquire_sema,
                            // vk::Semaphore::null(),
                            vk::Fence::null(),
                        )
                    }
                    .unwrap();
                    let dst_image = state.images[present_idx as usize];
                    let dst_image_layout = &mut state.image_layouts[present_idx as usize];

                    // dbg!(unsafe { *frame.as_ptr() });
                    let src_image = unsafe { *frame.as_ptr() }.image;
                    let src_create_info = unsafe { *frame.as_ptr() }.create_info;
                    assert!(src_create_info
                        .usage
                        .contains(vk::ImageUsageFlags::TRANSFER_SRC));
                    // TODO: Compare .image_create_info with our swapchain image!

                    let src_barrier = &mut unsafe { *frame.as_ptr() }.barrier;
                    assert_eq!(
                        src_barrier.parent.flags,
                        gst_vulkan::ffi::GST_VULKAN_BARRIER_FLAG_NONE
                    );
                    assert_eq!(
                        src_barrier.parent.type_,
                        gst_vulkan::ffi::GST_VULKAN_BARRIER_TYPE_IMAGE
                    );

                    let mut wait_semas = vec![acquire_sema];
                    let mut wait_values = vec![0];
                    let mut signal_semas = vec![present_sema];
                    let mut signal_values = vec![0];

                    if src_barrier.parent.semaphore != vk::Semaphore::null() {
                        // cfg!(feature = "v1_24") {
                        wait_semas.push(src_barrier.parent.semaphore);
                        wait_values.push(src_barrier.parent.semaphore_value);

                        // TODO: This is not currently useful because Gst bumps the barrier in parallel from a different thread (that's invalid!)
                        // signal_semas.push(src_barrier.parent.semaphore);
                        // src_barrier.parent.semaphore_value += 1;
                        // signal_values.push(src_barrier.parent.semaphore_value);
                    }

                    let image_copy = vk::ImageCopy {
                        src_subresource: vk::ImageSubresourceLayers {
                            aspect_mask: vk::ImageAspectFlags::COLOR,
                            mip_level: 0,
                            base_array_layer: 0,
                            layer_count: 1,
                        },
                        src_offset: Default::default(),
                        dst_subresource: vk::ImageSubresourceLayers {
                            aspect_mask: vk::ImageAspectFlags::COLOR,
                            mip_level: 0,
                            base_array_layer: 0,
                            layer_count: 1,
                        },
                        dst_offset: Default::default(),
                        extent: src_create_info.extent,
                    };

                    let &[cmdbuf] = unsafe {
                        device.allocate_command_buffers(
                            &vk::CommandBufferAllocateInfo::builder()
                                .command_pool(transfer_cmd_pool)
                                .command_buffer_count(1),
                        )
                    }
                    .unwrap()
                    .as_slice() else {
                        unreachable!()
                    };

                    unsafe {
                        device.begin_command_buffer(
                            cmdbuf,
                            &vk::CommandBufferBeginInfo::builder()
                                .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT),
                        )
                    }
                    .unwrap();

                    let subresource_range = vk::ImageSubresourceRange {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    };

                    // Transition source image
                    // let og_layout = src_barrier.image_layout;
                    if src_barrier.image_layout != vk::ImageLayout::TRANSFER_SRC_OPTIMAL
                        || src_barrier.image_layout != vk::ImageLayout::GENERAL
                    {
                        let (src_image_layout, src_stage_mask, src_access_mask) = if src_barrier
                            .image_layout
                            == vk::ImageLayout::UNDEFINED
                        {
                            // warn!("Image layout is empty, assuming unset barrier");
                            (
                                // Don't discard by using UNDEFINED... GStreamer only tells us this since 1.24!
                                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                                vk::PipelineStageFlags::TOP_OF_PIPE,
                                vk::AccessFlags::empty(),
                            )
                        } else {
                            (
                                src_barrier.image_layout,
                                vk::PipelineStageFlags::from_raw(
                                    src_barrier.parent.pipeline_stages as u32,
                                ),
                                vk::AccessFlags::from_raw(src_barrier.parent.access_flags as u32),
                            )
                        };
                        unsafe {
                            device.cmd_pipeline_barrier(
                                cmdbuf,
                                src_stage_mask,
                                vk::PipelineStageFlags::TRANSFER,
                                vk::DependencyFlags::empty(),
                                &[],
                                &[],
                                std::slice::from_ref(&vk::ImageMemoryBarrier {
                                    src_access_mask,
                                    dst_access_mask: vk::AccessFlags::TRANSFER_READ,
                                    old_layout: src_image_layout,
                                    new_layout: vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                                    // src_queue_family_index: todo!(),
                                    // dst_queue_family_index: todo!(),
                                    image: src_image,
                                    // TODO: Specify the range that we want?
                                    subresource_range, // : src_barrier.subresource_range,
                                    ..Default::default()
                                }),
                            )
                        }

                        src_barrier.parent.pipeline_stages =
                            vk::PipelineStageFlags::TRANSFER.as_raw() as _;
                        src_barrier.parent.access_flags =
                            vk::AccessFlags::TRANSFER_READ.as_raw() as _;
                        // Layout is changed back below!
                        // src_barrier.image_layout = vk::ImageLayout::TRANSFER_SRC_OPTIMAL;
                    }

                    unsafe {
                        device.cmd_pipeline_barrier(
                            cmdbuf,
                            // Present
                            // Wait for nothing
                            vk::PipelineStageFlags::TOP_OF_PIPE,
                            vk::PipelineStageFlags::TRANSFER,
                            vk::DependencyFlags::empty(),
                            &[],
                            &[],
                            std::slice::from_ref(&vk::ImageMemoryBarrier {
                                src_access_mask: vk::AccessFlags::empty(),
                                dst_access_mask: vk::AccessFlags::TRANSFER_WRITE,
                                // This will be UNDEFINED the first time, PRESENT_SRC thereafter
                                old_layout: *dst_image_layout,
                                new_layout: vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                                // src_queue_family_index: todo!(),
                                // dst_queue_family_index: todo!(),
                                image: dst_image,
                                // TODO: Specify the range that we want?
                                subresource_range, // : src_barrier.subresource_range,
                                ..Default::default()
                            }),
                        )
                    };
                    unsafe {
                        device.cmd_copy_image(
                            // TODO: This command buffer should be shared with the rest of the pipeline, to just copy into a presentable image at once.
                            // Alternatively we could provide these transfer-able swapchain images on our sink pad, depending on possible image usages?
                            cmdbuf,
                            src_image,
                            // src_barrier.image_layout,
                            vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                            dst_image,
                            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                            std::slice::from_ref(&image_copy),
                        )
                    };

                    // Put present image back in present layout
                    unsafe {
                        device.cmd_pipeline_barrier(
                            cmdbuf,
                            vk::PipelineStageFlags::TRANSFER,
                            // Synchronized with semaphores
                            vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                            vk::DependencyFlags::empty(),
                            &[],
                            &[],
                            std::slice::from_ref(&vk::ImageMemoryBarrier {
                                src_access_mask: vk::AccessFlags::TRANSFER_WRITE,
                                dst_access_mask: vk::AccessFlags::empty(),
                                old_layout: vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                                new_layout: vk::ImageLayout::PRESENT_SRC_KHR,
                                // src_queue_family_index: todo!(),
                                // dst_queue_family_index: todo!(),
                                image: dst_image,
                                // TODO: Specify the range that we want?
                                subresource_range, // : src_barrier.subresource_range,
                                ..Default::default()
                            }),
                        )
                    };

                    *dst_image_layout = vk::ImageLayout::PRESENT_SRC_KHR;

                    // Put source image back in TRANSFER_DST_OPTIMAL as expected by GST.
                    // TODO: Communicate this via barrier info!
                    unsafe {
                        device.cmd_pipeline_barrier(
                            cmdbuf,
                            vk::PipelineStageFlags::TRANSFER,
                            // No clue how GStreamer will use/recycle this texture after we're done with it
                            vk::PipelineStageFlags::TRANSFER,
                            vk::DependencyFlags::empty(),
                            &[],
                            &[],
                            std::slice::from_ref(&vk::ImageMemoryBarrier {
                                src_access_mask: vk::AccessFlags::TRANSFER_WRITE,
                                dst_access_mask: vk::AccessFlags::empty(),
                                old_layout: vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                                // GStreamer seems to reuse this image
                                new_layout: vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                                // src_queue_family_index: todo!(),
                                // dst_queue_family_index: todo!(),
                                image: src_image,
                                // TODO: Specify the range that we want?
                                subresource_range, // : src_barrier.subresource_range,
                                ..Default::default()
                            }),
                        )
                    };

                    // TODO: Remove this, and only update a few fields.
                    // 1. we already mutably bump and signal the fence above
                    // 2. We change the layout back
                    // 3. We might however need to update the stage with our stage flags

                    // *src_barrier = gst_vulkan::ffi::GstVulkanBarrierImageInfo {
                    //     parent: gst_vulkan::ffi::GstVulkanBarrierMemoryInfo {
                    //         type_: gst_vulkan::ffi::GST_VULKAN_BARRIER_TYPE_IMAGE,
                    //         flags: gst_vulkan::ffi::GST_VULKAN_BARRIER_FLAG_NONE,
                    //         queue: gst_queue.as_ptr(), // TODO
                    //         pipeline_stages: vk::PipelineStageFlags::TRANSFER.as_raw() as u64,
                    //         access_flags: vk::AccessFlags::empty().as_raw() as u64,
                    //         semaphore: original sema,
                    //         semaphore_value: original value + 1,
                    //         _reserved: [std::ptr::null_mut(); 4],
                    //     },
                    //     image_layout: vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                    //     subresource_range,
                    // };

                    // if og_layout != src_barrier.image_layout {
                    //     src_barrier.image_layout = og_layout;
                    // }

                    unsafe { device.end_command_buffer(cmdbuf) }.unwrap();

                    let mut timeline_sema_submit_info = vk::TimelineSemaphoreSubmitInfo::builder()
                        .wait_semaphore_values(&wait_values)
                        .signal_semaphore_values(&signal_values);

                    let submit_info = vk::SubmitInfo::builder()
                        .command_buffers(std::slice::from_ref(&cmdbuf))
                        .wait_semaphores(&wait_semas)
                        .wait_dst_stage_mask(std::slice::from_ref(
                            &vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                        ))
                        .signal_semaphores(&signal_semas)
                        .push_next(&mut timeline_sema_submit_info);

                    // TODO: Do we need to extend the image barrier here to clarify that we:
                    // 1. (TODO: Should!) put it in transfer src optimal (or read the current layout)
                    // 2. Have been reading data from it
                    // 3. Transition it back
                    // 4. Signal a semaphore indicating that we're done with the image
                    // 5. ... shared queues ... (TODO: Reuse the same queue as the Pipeline)

                    gst_queue.submit_lock();

                    unsafe {
                        device.queue_submit(
                            transfer_queue,
                            std::slice::from_ref(&submit_info),
                            vk::Fence::null(),
                        )
                    }
                    .unwrap();

                    unsafe {
                        swcl.queue_present(
                            transfer_queue,
                            &vk::PresentInfoKHR::builder()
                                .swapchains(std::slice::from_ref(&state.swapchain))
                                .image_indices(std::slice::from_ref(&present_idx))
                                // TODO: Actually wait for the copy to complete?
                                .wait_semaphores(std::slice::from_ref(&present_sema)),
                        )
                    }
                    .unwrap();

                    gst_queue.submit_unlock();
                } else {
                    println!("No state in redraw?")
                }
            }
            // windowed_context.swap_buffers().unwrap();
        }
    })?)
}
